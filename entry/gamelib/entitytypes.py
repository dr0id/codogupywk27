# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'entitytypes.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES
# LOSS OF USE, DATA, OR PROFITS OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The entity types values.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from gamelib.settings import KIND_TOWER_NORMAL, KIND_TOWER_SNIPER, KIND_TOWER_MACHINE_GUN, KIND_TOWER_FAN, KIND_E, \
    KIND_F, KIND_BATTERY, KIND_CHIPS_FACTORY, \
    KIND_SCIENCE, KIND_UPGRADE, KIND_CRYPTO_CHIPS_STORAGE_POOL, KIND_ENERGY_STORAGE_POOL, KIND_POWER, KIND_BULLET_A

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class _Building(object):
    # build properties
    build_cost = 0
    build_time = 0.0  # [s]
    # building properties
    kind = None
    level = 0
    radius = 0
    health = 0
    height = 0
    energy_consumption = 0.0  # [J/s]
    energy_production = 0.0  # [J/s]
    crypto_chips_earn_rate = 0.0  # [cc/s]
    # battery
    capacity = 0
    # tower properties
    fire_cool_time = 1  # [s] how long it takes to cool down
    fire_cool_count = 1  # after how many shots it goes to cool down
    fire_energy = 0  # [J]
    fire_rate = 0  # [shots/s]
    fire_range = 0  # units
    fire_cost = 0  # [CryptoChips/shot]
    fire_target_count = 1  # how many targets are shot simultaneously, fan or not
    # bullet properties
    bullet_damage = 1
    bullet_speed = 1
    bullet_kind = None


class TowerNormal0(_Building):
    name = 'Normal Gun Tower'
    level = 0
    build_cost = 1
    build_time = 1.0  # [s]
    energy_consumption = 1.0  # [J/s]
    health = 100
    height = 30
    kind = KIND_TOWER_NORMAL
    radius = 20

    # tower properties
    fire_cool_time = 1  # [s] how long it takes to cool down
    fire_cool_count = 1  # after how many shots it goes to cool down
    fire_energy = 1  # [J]
    fire_rate = 1  # [shots/s]
    fire_range = 40  # units
    fire_cost = 0.5  # [CryptoChips/shot]
    fire_target_count = 1  # how many targets are shot simultaneously, fan or not
    # bullet properties
    bullet_damage = 1
    bullet_speed = 150
    bullet_kind = KIND_BULLET_A


class TowerNormal1(TowerNormal0):
    level = 1
    build_cost = 1.15
    fire_range = 50  # units
    fire_rate = 30  # [shots/s]
    fire_cost = 0.6  # [CryptoChips/shot]


class TowerNormal2(TowerNormal1):
    level = 2
    build_cost = 1.3
    energy_consumption = 1.5  # [J/s]
    fire_cool_time = 0.94  # [s] how long it takes to cool down
    fire_cost = 0.7  # [CryptoChips/shot]


class TowerNormal3(TowerNormal2):
    level = 3
    build_cost = 1.45
    build_time = 1.5  # [s]
    fire_range = 56  # units
    fire_cool_time = 0.88  # [s] how long it takes to cool down
    fire_cost = 0.8  # [CryptoChips/shot]


class TowerNormal4(TowerNormal3):
    level = 4
    build_cost = 1.6
    fire_cool_time = 0.82  # [s] how long it takes to cool down
    fire_cost = 0.9  # [CryptoChips/shot]


class TowerNormal5(TowerNormal4):
    level = 5
    build_cost = 1.75
    fire_cool_time = 0.76  # [s] how long it takes to cool down
    fire_cost = 1.0  # [CryptoChips/shot]


class TowerNormal6(TowerNormal5):
    level = 6
    build_cost = 2
    build_time = 2.0  # [s]
    energy_consumption = 2.0  # [J/s]
    # tower properties
    fire_cool_time = 0.74  # [s] how long it takes to cool down
    fire_energy = 1  # [J]
    fire_rate = 2  # [shots/s]
    fire_range = 66  # units
    fire_cost = 1.1  # [CryptoChips/shot]
    # bullet properties
    bullet_damage = 2


_tower_a_levels = [TowerNormal0, TowerNormal1, TowerNormal2, TowerNormal3, TowerNormal4, TowerNormal5, TowerNormal6]


class TowerSniper0(_Building):
    name = 'Sniper Tower'
    level = 0
    kind = KIND_TOWER_SNIPER
    build_cost = 15
    build_time = 2.0  # [s]
    energy_consumption = 0.0  # [J/s]
    health = 100
    height = 50
    radius = 20

    # tower properties
    fire_cool_time = 2  # [s] how long it takes to cool down
    fire_cool_count = 1  # after how many shots it goes to cool down
    fire_energy = 5  # [J]
    fire_rate = 1  # [shots/s]
    fire_range = 100  # units
    fire_cost = 2  # [CryptoChips/shot]
    fire_target_count = 1  # how many targets are shot simultaneously, fan or not
    # bullet properties
    bullet_damage = 20
    bullet_speed = 200
    bullet_kind = KIND_BULLET_A


class TowerSniper1(TowerSniper0):
    level = 1
    build_cost = 16
    build_time = 2.0  # [s]
    fire_range = 110  # units
    fire_cost = 3  # [CryptoChips/shot]


class TowerSniper2(TowerSniper1):
    level = 2
    build_cost = 17
    build_time = 2.5  # [s]
    fire_energy = 4  # [J]
    bullet_damage = 25
    fire_cool_time = 3  # [s] how long it takes to cool down
    fire_cost = 4  # [CryptoChips/shot]


class TowerSniper3(TowerSniper2):
    level = 3
    build_cost = 17
    build_time = 2.5  # [s]
    bullet_speed = 250
    fire_range = 120  # units
    bullet_damage = 50
    fire_cost = 5  # [CryptoChips/shot]


class TowerSniper4(TowerSniper3):
    level = 4
    bullet_damage = 60
    fire_cool_time = 3.5  # [s] how long it takes to cool down
    fire_cost = 6  # [CryptoChips/shot]


class TowerSniper5(TowerSniper4):
    level = 5
    fire_cool_time = 4  # [s] how long it takes to cool down
    fire_range = 150  # units

    bullet_damage = 80
    fire_cost = 7  # [CryptoChips/shot]


class TowerSniper6(TowerSniper5):
    level = 6

    bullet_speed = 300
    bullet_damage = 100
    fire_cool_time = 5  # [s] how long it takes to cool down
    fire_cost = 8  # [CryptoChips/shot]


_tower_b_levels = [TowerSniper0, TowerSniper1, TowerSniper2, TowerSniper3, TowerSniper4, TowerSniper5, TowerSniper6]


class TowerMachineGun0(_Building):
    name = 'Machine Gun Tower'
    level = 0
    kind = KIND_TOWER_MACHINE_GUN
    build_cost = 2
    build_time = 1.0  # [s]
    energy_consumption = 0.0  # [J/s]
    health = 100
    height = 30
    radius = 20

    # tower properties
    fire_cool_time = 2.5  # [s] how long it takes to cool down
    fire_cool_count = 5  # after how many shots it goes to cool down
    fire_rate = 5  # [shots/s]
    fire_energy = 1  # [J]
    fire_range = 40  # units
    fire_cost = 0.1  # [CryptoChips/shot]
    fire_target_count = 1  # how many targets are shot simultaneously, fan or not
    # bullet properties
    bullet_damage = 0.5  # orig: 1
    bullet_speed = 150
    bullet_kind = KIND_BULLET_A


class TowerMachineGun1(TowerMachineGun0):
    level = 1
    fire_cool_time = 3  # [s] how long it takes to cool down
    fire_cool_count = 7  # after how many shots it goes to cool down
    fire_rate = 7  # [shots/s]
    fire_cost = 0.12  # [CryptoChips/shot]


class TowerMachineGun2(TowerMachineGun1):
    level = 2
    fire_range = 50  # units
    fire_rate = 9  # [shots/s]
    fire_cost = 0.13  # [CryptoChips/shot]


class TowerMachineGun3(TowerMachineGun2):
    level = 3
    fire_cool_time = 3  # [s] how long it takes to cool down
    fire_cool_count = 8  # after how many shots it goes to cool down
    fire_rate = 11  # [shots/s]
    build_time = 1.5  # [s]
    fire_cost = 0.14  # [CryptoChips/shot]


class TowerMachineGun4(TowerMachineGun3):
    level = 4
    fire_range = 60  # units
    fire_cool_time = 3  # [s] how long it takes to cool down
    fire_rate = 14  # [shots/s]
    fire_cost = 0.15  # [CryptoChips/shot]


class TowerMachineGun5(TowerMachineGun4):
    level = 5
    fire_rate = 17  # [shots/s]
    fire_cost = 0.16  # [CryptoChips/shot]


class TowerMachineGun6(TowerMachineGun5):
    level = 6
    fire_range = 70  # units
    fire_cool_time = 4  # [s] how long it takes to cool down
    fire_cool_count = 9  # after how many shots it goes to cool down
    fire_rate = 20  # [shots/s]
    build_time = 2.0  # [s]
    fire_cost = 0.17  # [CryptoChips/shot]


_tower_c_levels = [TowerMachineGun0, TowerMachineGun1, TowerMachineGun2, TowerMachineGun3, TowerMachineGun4,
                   TowerMachineGun5, TowerMachineGun6]


class TowerFan0(_Building):
    name = 'Fan Tower'
    level = 0
    kind = KIND_TOWER_FAN
    build_cost = 0
    build_time = 0.5  # [s]
    energy_consumption = 0.0  # [J/s]
    health = 100
    height = 30
    radius = 20

    # tower properties
    fire_cool_time = 0.5  # [s] how long it takes to cool down
    fire_cool_count = 1  # after how many shots it goes to cool down
    fire_energy = 1  # [J]
    fire_rate = 1  # [shots/s]
    fire_range = 40  # units
    fire_cost = 0.5  # [CryptoChips/shot] orig: 1.0
    fire_target_count = 3  # how many targets are shot simultaneously, fan or not
    # bullet properties
    bullet_damage = 0.5
    bullet_speed = 150
    bullet_kind = KIND_BULLET_A


class TowerFan1(TowerFan0):
    level = 1
    build_time = 0.7  # [s]
    fire_target_count = 4  # how many targets are shot simultaneously, fan or not


class TowerFan2(TowerFan1):
    level = 2
    build_time = 0.8  # [s]
    fire_target_count = 5  # how many targets are shot simultaneously, fan or not
    fire_cool_time = 1  # [s] how long it takes to cool down
    fire_range = 45  # units


class TowerFan3(TowerFan2):
    level = 3
    build_time = 0.9  # [s]
    fire_target_count = 6  # how many targets are shot simultaneously, fan or not
    fire_cost = 2.0  # [CryptoChips/shot]
    bullet_damage = 0.5  # 0.75


class TowerFan4(TowerFan3):
    level = 4
    build_time = 1.1  # [s]
    fire_target_count = 7  # how many targets are shot simultaneously, fan or not
    fire_cool_time = 1.5  # [s] how long it takes to cool down
    fire_range = 50  # units


class TowerFan5(TowerFan4):
    level = 5
    build_time = 1.3  # [s]
    fire_target_count = 8  # how many targets are shot simultaneously, fan or not
    fire_cool_time = 2  # [s] how long it takes to cool down


class TowerFan6(TowerFan5):
    level = 6
    build_time = 1.5  # [s]
    fire_target_count = 9  # how many targets are shot simultaneously, fan or not
    fire_cool_time = 3  # [s] how long it takes to cool down
    fire_cost = 3.0  # [CryptoChips/shot]
    fire_range = 60  # units
    bullet_damage = 0.5  # 1


_tower_d_levels = [TowerFan0, TowerFan1, TowerFan2, TowerFan3, TowerFan4, TowerFan5, TowerFan6]


class TowerE0(_Building):
    name = 'Tower E'
    level = 0
    kind = KIND_E
    build_cost = 0
    build_time = 0.0  # [s]
    energy_consumption = 0.0  # [J/s]
    fire_energy = 000  # [J]
    fire_rate = 100000  # [shots/s]
    fire_range = 10  # units
    fire_cost = 1  # [CryptoChips/shot]
    health = 100
    height = 30
    radius = 10


class TowerE1(TowerE0):
    level = 1


class TowerE2(TowerE1):
    level = 2


class TowerE3(TowerE2):
    level = 3


class TowerE4(TowerE3):
    level = 4


class TowerE5(TowerE4):
    level = 5


class TowerE6(TowerE5):
    level = 6


_tower_e_levels = [TowerE0, TowerE1, TowerE2, TowerE3, TowerE4, TowerE5, TowerE6]


class TowerF0(_Building):
    name = 'Tower F'
    level = 0
    kind = KIND_F
    build_cost = 0
    build_time = 0.0  # [s]
    energy_consumption = 0.0  # [J/s]
    fire_energy = 000  # [J]
    fire_rate = 100000  # [shots/s]
    fire_range = 10  # units
    fire_cost = 1  # [CryptoChips/shot]
    health = 100
    height = 30
    radius = 10


class TowerF1(TowerF0):
    level = 1


class TowerF2(TowerF1):
    level = 2


class TowerF3(TowerF2):
    level = 3


class TowerF4(TowerF3):
    level = 4


class TowerF5(TowerF4):
    level = 5


class TowerF6(TowerF5):
    level = 6


_tower_f_levels = [TowerF0, TowerF1, TowerF2, TowerF3, TowerF4, TowerF5, TowerF6]


class UpgradeCenter0(_Building):
    name = 'Upgrade Center'
    level = 0
    kind = KIND_UPGRADE
    build_cost = 10
    build_time = 2.0  # [s]
    energy_consumption = 2.0  # [J/s]
    health = 100
    height = 30
    radius = 30


class UpgradeCenter1(UpgradeCenter0):
    level = 1
    health = 105


class UpgradeCenter2(UpgradeCenter1):
    level = 2
    health = 110


class UpgradeCenter3(UpgradeCenter2):
    level = 3
    health = 120


class UpgradeCenter4(UpgradeCenter3):
    level = 4
    health = 130


class UpgradeCenter5(UpgradeCenter4):
    level = 5
    health = 140


class UpgradeCenter6(UpgradeCenter5):
    level = 6
    health = 150


_upgrade_center_levels = [UpgradeCenter0, UpgradeCenter1, UpgradeCenter2, UpgradeCenter3, UpgradeCenter4,
                          UpgradeCenter5,
                          UpgradeCenter6]


class PowerStation0(_Building):
    name = 'Power Station'
    level = 0
    kind = KIND_POWER
    build_cost = 10
    build_time = 2.0  # [s]
    energy_production = 1.0  # [J/s]
    health = 100
    height = 30
    radius = 20


class PowerStation1(PowerStation0):
    level = 1
    build_time = 1.0  # [s]
    energy_production = 2.0  # [J/s]
    build_cost = 5


class PowerStation2(PowerStation1):
    level = 2
    energy_production = 3.0  # [J/s]


class PowerStation3(PowerStation2):
    level = 3
    energy_production = 4.0  # [J/s]


class PowerStation4(PowerStation3):
    level = 4
    energy_production = 5.0  # [J/s]


class PowerStation5(PowerStation4):
    level = 5
    energy_production = 6.0  # [J/s]


class PowerStation6(PowerStation5):
    level = 6
    energy_production = 7.0  # [J/s]


_power_station_levels = [PowerStation0, PowerStation1, PowerStation2, PowerStation3, PowerStation4, PowerStation5,
                         PowerStation6]


class ScienceCenter0(_Building):
    name = 'Science Center'
    level = 0
    kind = KIND_SCIENCE
    build_cost = 40
    build_time = 60  # [s]
    energy_consumption = 2.0  # [J/s]
    health = 100
    height = 30
    radius = 15


class ScienceCenter1(ScienceCenter0):
    level = 1
    build_time = 40
    build_cost = 5


class ScienceCenter2(ScienceCenter1):
    level = 2
    build_time = 40  # [s]
    energy_consumption = 1.5  # [J/s]


class ScienceCenter3(ScienceCenter2):
    level = 3
    build_time = 40


class ScienceCenter4(ScienceCenter3):
    level = 4
    build_time = 40  # [s]
    energy_consumption = 1.0  # [J/s]
    build_cost = 10


class ScienceCenter5(ScienceCenter4):
    level = 5
    build_time = 40  # [s]
    health = 120
    build_cost = 12
    energy_consumption = 2.0  # [J/s]


class ScienceCenter6(ScienceCenter5):
    level = 6
    health = 140
    build_time = 40
    build_cost = 15
    energy_consumption = 4.0  # [J/s]


_science_center_levels = [ScienceCenter0, ScienceCenter1, ScienceCenter2, ScienceCenter3, ScienceCenter4,
                          ScienceCenter5,
                          ScienceCenter6]


class ChipsFactory0(_Building):
    name = 'Chipworks Factory'
    level = 0
    build_cost = 15
    build_time = 2.0  # [s]
    health = 130
    height = 30
    crypto_chips_earn_rate = 0.7  # [cc/s] orig: 1.0
    energy_production = 1.0  # [J/s]
    radius = 25
    kind = KIND_CHIPS_FACTORY


class ChipsFactory1(ChipsFactory0):
    build_cost = 2
    level = 1
    build_time = 10  # 2.5  # [s]
    health = 130
    crypto_chips_earn_rate = 2.0  # [cc/s]
    energy_production = 1.0  # [J/s]


class ChipsFactory2(ChipsFactory1):
    level = 2
    build_cost = 20
    build_time = 20  # 2.5  # [s]
    crypto_chips_earn_rate = 3.0  # [cc/s]
    health = 150
    energy_production = 2.0  # [J/s]


class ChipsFactory3(ChipsFactory2):
    level = 3
    build_cost = 20
    build_time = 30  # 3  # [s]
    crypto_chips_earn_rate = 4.0  # [cc/s]
    health = 170
    energy_production = 3.0  # [J/s]


class ChipsFactory4(ChipsFactory3):
    level = 4
    height = 40
    radius = 30
    build_cost = 22
    build_time = 40  # 3.5  # [s]
    crypto_chips_earn_rate = 5.0  # [cc/s]
    health = 200
    energy_production = 4.0  # [J/s]


class ChipsFactory5(ChipsFactory4):
    level = 5
    build_cost = 22
    build_time = 50  # 4  # [s]
    crypto_chips_earn_rate = 7.0  # [cc/s]
    energy_production = 5.0  # [J/s]
    health = 300


class ChipsFactory6(ChipsFactory5):
    level = 6
    build_cost = 25
    build_time = 60  # 5.0  # [s]
    crypto_chips_earn_rate = 8.0  # [cc/s]
    health = 400
    energy_production = 6.0  # [J/s]


_chips_factory_levels = [ChipsFactory0, ChipsFactory1, ChipsFactory2, ChipsFactory3, ChipsFactory4, ChipsFactory5,
                         ChipsFactory6]


class Battery0(_Building):
    name = 'Battery Station'
    level = 0
    kind = KIND_BATTERY
    build_cost = 1
    build_time = 2.0  # [s]
    capacity = 2.0  # [J/s]
    health = 100
    height = 30
    radius =20


class Battery1(Battery0):
    level = 1
    capacity = 10  # [J/s]
    build_cost = 5


class Battery2(Battery1):
    level = 2
    capacity = 20  # [J/s]
    build_time = 5
    build_cost = 7


class Battery3(Battery2):
    level = 3
    capacity = 30  # [J/s]
    build_time = 10
    build_cost = 8


class Battery4(Battery3):
    level = 4
    capacity = 40  # [J/s]
    build_time = 20
    build_cost = 9


class Battery5(Battery4):
    level = 5
    capacity = 50  # [J/s]
    build_time = 25
    build_cost = 10


class Battery6(Battery5):
    level = 6
    capacity = 66  # [J/s]
    build_time = 30
    build_cost = 12


_battery_levels = [Battery0, Battery1, Battery2, Battery3, Battery4, Battery5,
                   Battery6]

entity_values = {
    KIND_TOWER_NORMAL: _tower_a_levels,
    KIND_TOWER_SNIPER: _tower_b_levels,
    KIND_TOWER_MACHINE_GUN: _tower_c_levels,
    KIND_TOWER_FAN: _tower_d_levels,
    KIND_E: _tower_e_levels,
    KIND_F: _tower_f_levels,
    KIND_UPGRADE: _upgrade_center_levels,
    KIND_POWER: _power_station_levels,
    KIND_SCIENCE: _science_center_levels,
    KIND_CHIPS_FACTORY: _chips_factory_levels,
    KIND_BATTERY: _battery_levels
}


def is_upgrade_available(kind, current_level):
    if kind not in entity_values:
        return False

    if current_level + 1 < len(entity_values[kind]):
        return True
        # return entity_values[kind][current_level + 1]
    return False


class _StoragePool(object):

    def __init__(self, capacity, amount, kind):
        self.capacity = capacity
        self.amount = amount
        self.kind = kind


class EnergyStoragePool(_StoragePool):

    def __init__(self):
        _StoragePool.__init__(self, 0, 0, KIND_ENERGY_STORAGE_POOL)


class CryptoChipsStoragePool(_StoragePool):

    def __init__(self):
        _StoragePool.__init__(self, 0, 0, KIND_CRYPTO_CHIPS_STORAGE_POOL)

    # note: capacity is irrelevant for crypto chips


logger.debug("imported")
