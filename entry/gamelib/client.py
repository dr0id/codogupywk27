# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'client.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
The client class.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random
from os import path

import pygame

from gamelib import settings, message_display, hudlight, hudthemes, pygametext, sfx, resource_sound
from gamelib.entitytypes import TowerNormal0, TowerSniper0, TowerMachineGun0, TowerFan0, TowerE0, TowerF0, \
    PowerStation0, ScienceCenter0, \
    Battery0, ChipsFactory0, UpgradeCenter0
from gamelib.eventing import event_dispatcher
from gamelib.resource_gfx import image_names
from gamelib.settings import gameplay_event_map, ACTION_TOGGLE_DEBUG_RENDER, ACTION_QUIT, EVT_QUIT, \
    music_ended_pygame_event, ACTION_MOUSE_MOTION, ACTION_MBUTTON1_DOWN, hud_screen_rect, main_screen_rect, \
    build_button_size, LAYER_SCROLL_BUTTONS, scroll_button_size, EVT_CHIPS_FACTORY_CREATED, \
    EVT_UNLOCKED, EVT_SCIENCE_CENTER_CREATED, LAYER_BUILD_BUTTONS, screen_width, ACTION_OTHER_MBUTTON_DOWN, \
    EVT_UI_SWITCHED_TO_SELECTION_MOUSE_MODE, EVT_UI_SWITCHED_TO_PLACING_MOUSE_MODE, LAYER_MOUSE_SPRITE, \
    EVT_BUILDING_PLACED, \
    EVT_TOWER_CREATED, EVT_UPGRADE_CENTER_CREATED, EVT_POWER_STATION_CREATED, EVT_BATTERY_CREATED, LAYER_TOWER, \
    EVT_FOCUS_GAINED, EVT_FOCUS_LOST, EVT_ANT_CREATED, EVT_TIME_INDICATOR_ADDED, EVT_TIME_INDICATOR_REMOVED, \
    LAYER_INDICATOR, LAYER_HEALTH, ACTION_UPGRADE_BUILDING, EVT_BUILDING_REMOVED, EVT_POOL_CREATED, LAYER_HUD, \
    KIND_ENERGY_STORAGE_POOL, KIND_ANT, KIND_BULLET_A, \
    EVT_BUILDING_UPGRADED, EVT_TYPE_UNLOCKED, EVT_NEW_SYSTEM_MESSAGE, EVT_SOUND_STARTED, EVT_BUILDING_HIT, \
    EVT_BULLETS_CREATED, LAYER_BULLET, EVT_BULLETS_DESTROYED, gfx_path, EVT_ANTS_DIED, EVT_OUT_OF_POWER, \
    EVT_POWER_RESTORED, EVT_NO_CHIPS_TO_FIRE, EVT_READY_TO_FIRE, EVT_TOWER_DESTROYED, EVT_UPGRADE_CENTER_DESTROYED, \
    EVT_POWER_STATION_DESTROYED, EVT_SCIENCE_CENTER_DESTROYED, EVT_CHIPS_FACTORY_DESTROYED, EVT_BATTERY_DESTROYED, \
    LAYER_TOWER_POWER_LOSS, LAYER_TOWER_LOW_CHIPS, LAYER_PATH, EVT_PATH_CREATED, EVT_WAVE_STARTED, EVT_ANT_TURNED, \
    EVT_BUILDING_PLACED_IN_WORLD
from gamelib.ui import Button, TrackingSprite, LockButton, LockSprite, RadiusSprite, IndicatorSprite, HealthBar, \
    LevelIndicator, PathDrawer, HudSprite, FireRangeSprite
from pyknic.mathematics import Vec3 as Vec, Point3 as Point
from pyknic.pyknic_pygame.spritesystem import Camera, DefaultRenderer, VectorSprite, Sprite
from pyknic.tweening import Tweener

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class Client(object):

    def __init__(self, music_player, space):
        self.music_player = music_player
        self._debug_render = False
        self.cam = Camera(main_screen_rect)
        self.cam.position = Point(main_screen_rect[2] // 2, main_screen_rect[3] // 2)
        self.cam_hud = Camera(hud_screen_rect)
        self.cam_hud.position = Point(hud_screen_rect[2] // 2, hud_screen_rect[3] // 2)
        self.renderer = DefaultRenderer()
        self.renderer.always_sort = True
        self.hud = DefaultRenderer()
        self.assigned_sprites = {}  # {ent: [sprites]}
        self.entity_sprite = {}  # {ent: sprite}
        self.power_sprites = {}
        self.chips_sprites = {}
        self.current_wave_num = "wait for it..."

        self._debug_sprites = []
        self._debug_hud_sprites = []

        # fixme: register them elsewhere?
        # registrations
        event_dispatcher.register_event_type(EVT_UNLOCKED)
        event_dispatcher.register_event_type(EVT_TYPE_UNLOCKED)
        event_dispatcher.register_event_type(EVT_UI_SWITCHED_TO_SELECTION_MOUSE_MODE)
        event_dispatcher.register_event_type(EVT_UI_SWITCHED_TO_PLACING_MOUSE_MODE)

        event_dispatcher.register_event_type(EVT_TOWER_CREATED)
        event_dispatcher.register_event_type(EVT_UPGRADE_CENTER_CREATED)
        event_dispatcher.register_event_type(EVT_POWER_STATION_CREATED)
        event_dispatcher.register_event_type(EVT_SCIENCE_CENTER_CREATED)
        event_dispatcher.register_event_type(EVT_CHIPS_FACTORY_CREATED)
        event_dispatcher.register_event_type(EVT_BATTERY_CREATED)

        event_dispatcher.register_event_type(EVT_BUILDING_PLACED)
        event_dispatcher.register_event_type(EVT_BUILDING_HIT)

        event_dispatcher.register_event_type(EVT_FOCUS_GAINED)
        event_dispatcher.register_event_type(EVT_FOCUS_LOST)

        event_dispatcher.register_event_type(EVT_ANT_CREATED)
        event_dispatcher.register_event_type(EVT_TIME_INDICATOR_ADDED)
        event_dispatcher.register_event_type(EVT_TIME_INDICATOR_REMOVED)
        event_dispatcher.register_event_type(ACTION_UPGRADE_BUILDING)
        event_dispatcher.register_event_type(EVT_BUILDING_REMOVED)
        event_dispatcher.register_event_type(EVT_POOL_CREATED)
        event_dispatcher.register_event_type(EVT_BUILDING_UPGRADED)

        event_dispatcher.register_event_type(EVT_NEW_SYSTEM_MESSAGE)
        event_dispatcher.register_event_type(EVT_WAVE_STARTED)
        event_dispatcher.register_event_type(EVT_BULLETS_CREATED)
        event_dispatcher.register_event_type(EVT_BULLETS_DESTROYED)
        event_dispatcher.register_event_type(EVT_ANTS_DIED)
        event_dispatcher.register_event_type(EVT_OUT_OF_POWER)
        event_dispatcher.register_event_type(EVT_POWER_RESTORED)
        event_dispatcher.register_event_type(EVT_NO_CHIPS_TO_FIRE)
        event_dispatcher.register_event_type(EVT_READY_TO_FIRE)

        event_dispatcher.register_event_type(EVT_TOWER_DESTROYED)
        event_dispatcher.register_event_type(EVT_UPGRADE_CENTER_DESTROYED)
        event_dispatcher.register_event_type(EVT_POWER_STATION_DESTROYED)
        event_dispatcher.register_event_type(EVT_SCIENCE_CENTER_DESTROYED)
        event_dispatcher.register_event_type(EVT_CHIPS_FACTORY_DESTROYED)
        event_dispatcher.register_event_type(EVT_BATTERY_DESTROYED)
        event_dispatcher.register_event_type(EVT_PATH_CREATED)
        event_dispatcher.register_event_type(EVT_ANT_TURNED)
        event_dispatcher.register_event_type(EVT_BUILDING_PLACED_IN_WORLD)

        self.sfx_handler = SfxHandler()

        # load resources elsewhere!
        self.images = {}
        for k, n in image_names.items():
            p = path.join(gfx_path, n)
            img = pygame.image.load(p).convert_alpha()
            img = pygame.transform.rotozoom(img, 0.0, settings.widget_scale)
            self.images[k] = img

        self.images[KIND_ANT] = pygame.image.load(path.join(gfx_path, image_names["ant"])).convert_alpha()

        self.images[KIND_BULLET_A] = pygame.image.load(path.join(gfx_path, image_names["bullet"])).convert_alpha()

        # img = pygame.Surface((5, 30))
        # img.fill((255, 255, 0))
        # self.images["power loss!"] = img
        self.images["power loss!"] = pygame.image.load(path.join(gfx_path, image_names["noPower"])).convert_alpha()

        # img = pygame.Surface((10, 40))
        # img.fill((255, 128, 0))
        # self.images["no chips to fire!"] = img
        self.images["no chips to fire!"] = pygame.image.load(
            path.join(gfx_path, image_names["noChips"])).convert_alpha()

        # r = pygame.Rect((0, 0), build_button_size).inflate(-20, -20)
        # surf = pygame.Surface(r.size, pygame.SRCALPHA)
        # surf.fill((255, 0, 0, 128))
        # self.images["locked"] = surf
        self.images["locked"] = pygame.image.load(path.join(gfx_path, image_names["locked"])).convert_alpha()
        self.images["scrollR"] = pygame.image.load(path.join(gfx_path, image_names["scrollR"])).convert_alpha()
        self.images["scrollL"] = pygame.transform.flip(self.images["scrollR"], True, False)

        # listeners
        event_dispatcher.add_listener(EVT_UI_SWITCHED_TO_SELECTION_MOUSE_MODE, self.on_switch_to_selection_mode)
        event_dispatcher.add_listener(EVT_UI_SWITCHED_TO_PLACING_MOUSE_MODE, self.on_switch_to_placing_mode)

        event_dispatcher.add_listener(EVT_TOWER_CREATED, self.on_tower_created)
        # event_dispatcher.add_listener(EVT_UPGRADE_CENTER_CREATED, self.on_update_center_created)
        # event_dispatcher.add_listener(EVT_POWER_STATION_CREATED, self.on_power_station_created)
        # event_dispatcher.add_listener(EVT_SCIENCE_CENTER_CREATED, self.on_science_center_created)
        # event_dispatcher.add_listener(EVT_CHIPS_FACTORY_CREATED, self.on_chips_factory_created)
        # event_dispatcher.add_listener(EVT_BATTERY_CREATED, self.on_battery_created)
        event_dispatcher.add_listener(EVT_BUILDING_PLACED_IN_WORLD, self.on_place_in_world)

        event_dispatcher.add_listener(EVT_BUILDING_UPGRADED, self.on_building_upgraded)

        event_dispatcher.add_listener(EVT_FOCUS_GAINED, self.on_focus_gained)
        event_dispatcher.add_listener(EVT_FOCUS_LOST, self.on_focus_lost)

        event_dispatcher.add_listener(EVT_ANT_CREATED, self.on_ant_created)
        event_dispatcher.add_listener(EVT_TIME_INDICATOR_ADDED, self.on_time_indicator_added)
        event_dispatcher.add_listener(EVT_TIME_INDICATOR_REMOVED, self.on_time_indicator_removed)
        event_dispatcher.add_listener(EVT_BUILDING_REMOVED, self.on_building_removed)
        event_dispatcher.add_listener(EVT_POOL_CREATED, self.on_pool_created)
        event_dispatcher.add_listener(EVT_BULLETS_CREATED, self.on_bullets_created)
        event_dispatcher.add_listener(EVT_BULLETS_DESTROYED, self.on_bullets_destroyed)
        event_dispatcher.add_listener(EVT_ANTS_DIED, self.on_ants_died)
        event_dispatcher.add_listener(EVT_OUT_OF_POWER, self.on_power_loss)
        event_dispatcher.add_listener(EVT_POWER_RESTORED, self.on_power_restore)
        event_dispatcher.add_listener(EVT_NO_CHIPS_TO_FIRE, self.on_not_enough_chips_to_fire)
        event_dispatcher.add_listener(EVT_READY_TO_FIRE, self.on_ready_to_fire)

        event_dispatcher.add_listener(EVT_TOWER_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_UPGRADE_CENTER_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_POWER_STATION_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_SCIENCE_CENTER_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_CHIPS_FACTORY_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_BATTERY_DESTROYED, self.on_building_destroyed)
        event_dispatcher.add_listener(EVT_PATH_CREATED, self.on_path_created)
        event_dispatcher.add_listener(EVT_WAVE_STARTED, self.on_wave_started)
        event_dispatcher.add_listener(EVT_ANT_TURNED, self.on_ant_turned)

        event_dispatcher.add_listener(EVT_UNLOCKED, self.on_unlocked)

        # buttons
        offset = Vec(scroll_button_size[0], 0)
        step = Vec(build_button_size[0], 0)
        build_tower_normal_button = LockButton(self._on_do, TowerNormal0)
        build_tower_sniper_button = LockButton(self._on_do, TowerSniper0)
        build_tower_machine_gun_button = LockButton(self._on_do, TowerMachineGun0)
        build_tower_fan_out_button = LockButton(self._on_do, TowerFan0)
        build_towerE_button = LockButton(self._on_do, TowerE0)
        build_towerF_button = LockButton(self._on_do, TowerF0)
        build_power_station_button = LockButton(self._on_do, PowerStation0)
        build_upgrade_center_button = LockButton(self._on_do, UpgradeCenter0)
        build_science_center_button = LockButton(self._on_do, ScienceCenter0)
        build_battery_button = LockButton(self._on_do, Battery0)
        build_chips_factory_button = LockButton(self._on_do, ChipsFactory0)

        self.build_buttons = [build_tower_normal_button,
                              build_power_station_button,
                              build_upgrade_center_button,
                              build_tower_sniper_button,
                              build_tower_machine_gun_button,
                              build_tower_fan_out_button,
                              # build_towerE_button,
                              # build_towerF_button,
                              build_science_center_button,
                              build_battery_button,
                              build_chips_factory_button,
                              ]

        button_scroll_left = Button(self._on_scroll_right, Point(*scroll_button_size) / 2)
        # surf = pygame.Surface(scroll_button_size)
        # surf.fill((255, 255, 0))
        surf = self.images["scrollL"]
        self.hud.add_sprite(TrackingSprite(surf, button_scroll_left, z_layer=LAYER_SCROLL_BUTTONS))

        self._visible_buttons_count = len(
            self.build_buttons)  # you know why, don't you? It's the *theme* of this pyweek!
        self._build_button_index = 0
        pos = Point(*build_button_size) / 2 + offset + step * self._visible_buttons_count - step / 2 + Vec(
            scroll_button_size[0] // 2, 0)
        button_scroll_right = Button(self._on_scroll_left, pos)
        # surf = pygame.Surface(scroll_button_size)
        # surf.fill((255, 255, 0))
        surf = self.images["scrollR"]
        self.hud.add_sprite(TrackingSprite(surf, button_scroll_right, z_layer=LAYER_SCROLL_BUTTONS))

        # rest of the area to show other info
        pos = button_scroll_right.position + Vec(scroll_button_size[0] // 2, 0)
        surf = pygame.Surface((screen_width - pos.x, scroll_button_size[1]))
        surf.fill((200, 100, 150))

        sprite = Sprite(surf, pos + Vec(surf.get_size()[0] // 2, 0), z_layer=LAYER_HUD)
        self.hud.add_sprite(sprite)
        self.hud_rect = surf.get_rect()
        self.hud_rect.center = sprite.position.as_xy_tuple(int)

        for idx, b in enumerate(self.build_buttons):
            b.position.copy_values(Point(*build_button_size) / 2 + offset + step * idx)
            # type depending image
            # surf = pygame.Surface(build_button_size)
            # surf.fill((255, 255, 255), surf.get_rect().inflate(-2, -2))
            surf = self.images.get((b._unlock_type.kind, -1), None)
            if surf is None:
                surf = pygame.Surface(build_button_size)
                surf.fill((255, 255, 255), surf.get_rect().inflate(-2, -2))
            # else:
            #     surf = pygame.transform.scale(surf, build_button_size)
            # inflate = pygame.Rect((0, 0), build_button_size).inflate(-1, -1)
            # inflate.topleft= (0, 0)
            # pygame.draw.rect(surf, (0, 0, 0), inflate, 1)
            self.hud.add_sprite(TrackingSprite(surf, b, z_layer=LAYER_BUILD_BUTTONS))

            # lock image (re-use image I guess)
            surf = self.images["locked"]
            self.hud.add_sprite(LockSprite(surf, b, z_layer=LAYER_BUILD_BUTTONS + 1))

        # Territories
        self.chipworks_rect = settings.chipworks_rect
        self.ant_country_rect = settings.ant_country_rect
        self.contested_lands_rect = settings.contested_lands_rect

        # mouse selection modes
        radius = 50 * settings.widget_scale
        surf = pygame.Surface((radius, radius), pygame.SRCALPHA)
        surf.fill((0, 0, 255, 128))
        placing_sprite = Sprite(surf, Point(0, 0), anchor='midbottom', z_layer=LAYER_MOUSE_SPRITE)
        self.current_mouse_mode = SelectionMode(self.renderer, self.hud)
        self.other_mouse_mode = PlacingMode(self.renderer, self.hud, placing_sprite, self.cam, space, self.images)

        self.tweener = Tweener()
        message_display.init_system(self.renderer, self.tweener)
        event_dispatcher.add_listener(EVT_NEW_SYSTEM_MESSAGE, message_display.add_system_message)

        self.clock = pygame.time.Clock()
        self.num_ants_died = 0
        self.debug_hud_on = False
        self.debug_hud = None
        self._make_debug_hud()
        self.info_hud = self._make_info_hud()
        h_spr = HudSprite(self.info_hud, Point(self.hud_rect.left, self.hud_rect.top), LAYER_HUD + 1)
        self.hud.add_sprite(h_spr)

    def _make_info_hud(self):
        hudlight.set_font_template('data/font/%s')
        theme = hudthemes.HUD_THEMES['verabd'].copy()
        theme.update(fontsize=16, color='midnightblue', gcolor=None, scolor=None, ocolor='white')
        info_hud = hudlight.HUD(**theme)
        info_hud.x = self.hud_rect.left
        info_hud.y = self.hud_rect.top
        info_hud.add("wave_num", "Wave: {}", self.current_wave_num, callback=lambda: self.current_wave_num)

        return info_hud

    def _make_debug_hud(self):
        hudlight.set_font_template('data/font/%s')
        theme = hudthemes.HUD_THEMES['verabd'].copy()
        theme.update(fontsize=16, color='midnightblue', gcolor=None, scolor=None, ocolor='white')
        self.debug_hud = hudlight.HUD(**theme)
        add_item = self.debug_hud.add

        def _get_fps(*args):
            return int(self.clock.get_fps())

        add_item('fps', 'FPS {}', _get_fps(), callback=_get_fps)

        def _get_num_ants(*args):
            return len([e for e in self.entity_sprite if e.kind == KIND_ANT]), self.num_ants_died

        add_item('num_ants', 'Ants {} Died {}', *_get_num_ants(), callback=_get_num_ants)

        def _get_num_bullets(*args):
            return len([e for e in self.entity_sprite if e.kind == KIND_BULLET_A])

        add_item('num_bullets', 'Bullets {}', _get_num_bullets(), callback=_get_num_bullets)

        def _get_pygametext_size(*args):
            return int(round(pygametext._surf_size_total / 2 ** 20))

        add_item('font_cache_size', 'Font cache {} MB', _get_pygametext_size(), callback=_get_pygametext_size)

    def _on_do(self, *args):
        logger.info("button clicked! {0}", args)
        event_dispatcher.fire(EVT_UI_SWITCHED_TO_PLACING_MOUSE_MODE, *args)

    def on_unlocked(self, event_type, button, unlock_type):
        logger.debug("unlocked! {}", unlock_type.name)
        # these three types are part of the tutorial; don't let them speak
        if unlock_type.name not in ('Normal Gun Tower', 'Power Station', 'Upgrade Center'):
            event_dispatcher.fire(EVT_NEW_SYSTEM_MESSAGE, 'Unlocked: {}'.format(unlock_type.name), True)

    def on_switch_to_selection_mode(self, *args):
        if isinstance(self.current_mouse_mode, SelectionMode):
            return
        logger.info("switching to selection mode {0}", args)
        self.current_mouse_mode, self.other_mouse_mode = self.other_mouse_mode, self.current_mouse_mode

    def on_switch_to_placing_mode(self, event, building_type, *args):
        if isinstance(self.current_mouse_mode, PlacingMode):
            self.current_mouse_mode.update_placing_sprite(building_type)
            return
        logger.info("switching to placing {0}, {1}, {2}", event, building_type, args)
        self.current_mouse_mode, self.other_mouse_mode = self.other_mouse_mode, self.current_mouse_mode
        self.current_mouse_mode.update_placing_sprite(building_type)

    def _on_scroll_left(self, *args):
        if self._build_button_index == len(self.build_buttons) - self._visible_buttons_count:
            return
        self._build_button_index += 1
        for b in self.build_buttons:
            b.position -= Vec(build_button_size[0], 0)

    def _on_scroll_right(self, *args):
        if self._build_button_index == 0:
            return
        self._build_button_index -= 1
        for b in self.build_buttons:
            b.position += Vec(build_button_size[0], 0)

    def on_tower_created(self, event_type, building, *args):
        # img = self.images[(building.kind, building.level)]
        # self._on_building_created(building, img)
        self.assigned_sprites.setdefault(building, []).append(
            FireRangeSprite(building, LAYER_TOWER - 1, False, (255, 0, 0)))

    # def on_update_center_created(self, event_type, building, *args):
    #     img = self.images[(building.kind, building.level)]
    #     self._on_building_created(building, img)
    #
    # def on_power_station_created(self, event_type, building, *args):
    #     img = self.images[(building.kind, building.level)]
    #     self._on_building_created(building, img)
    #
    # def on_science_center_created(self, event_type, building, *args):
    #     img = self.images[(building.kind, building.level)]
    #     self._on_building_created(building, img)
    #
    # def on_chips_factory_created(self, event_type, building, *args):
    #     img = self.images[(building.kind, building.level)]
    #     self._on_building_created(building, img)
    #
    # def on_battery_created(self, event_type, building, *args):
    #     img = self.images[(building.kind, building.level)]
    #     self._on_building_created(building, img)

    def on_place_in_world(self, event_type, building, *args):
        img = self.images[(building.kind, building.level)]
        self._on_building_created(building, img)

    def _on_building_created(self, building, img):
        if building in self.entity_sprite:
            self.on_building_removed(None, building)

        # actual building sprite
        # DON'T CHANGE: see hack in SelectionMode._hit_by_radius first!!!
        anchor = Vec(0, img.get_rect()[3] // 2 - building.radius)
        spr = TrackingSprite(img, building, anchor, z_layer=LAYER_TOWER)
        self.renderer.add_sprite(spr)
        self.entity_sprite[building] = spr

        # radius
        # radius_sprite = RadiusSprite(building, LAYER_TOWER + 1, True, building.radius)
        # self.assigned_sprites.setdefault(building, []).append(radius_sprite)
        # self._debug_sprites.append(radius_sprite)

        # health bar
        health_bar = HealthBar(building, LAYER_HEALTH)
        health_bar.visible = False
        self.assigned_sprites.setdefault(building, []).append(health_bar)

        # level indicator
        level_indicator = LevelIndicator(building, LAYER_HEALTH)
        level_indicator.visible = False
        self.assigned_sprites.setdefault(building, []).append(level_indicator)

    def on_building_upgraded(self, event_type, building):
        if building not in self.entity_sprite:
            logger.error("building not found in self.entity_sprites")
            return

        img = self.entity_sprite[building].image
        img = self.images.get((building.kind, building.level), img)

        # remove sprite
        old_sprite = self.entity_sprite[building]
        del self.entity_sprite[building]
        self.renderer.remove_sprite(old_sprite)

        # add new sprite
        anchor = Vec(0, img.get_rect()[3] // 2 - building.radius)
        spr = TrackingSprite(img, building, anchor, z_layer=LAYER_TOWER)
        self.renderer.add_sprite(spr)
        self.entity_sprite[building] = spr

    def on_focus_gained(self, event_tpye, spr):
        self._set_assigned_visible(spr, True)

    def on_focus_lost(self, event_type, spr):
        self._set_assigned_visible(spr, False)

    def _set_assigned_visible(self, spr, visible):
        ent = getattr(spr, 'tracked_entity', None)
        if ent is not None:
            assigned = self.assigned_sprites.get(ent, None)
            if assigned is not None:
                for a in assigned:
                    a.visible = visible

    def on_ant_created(self, event_type, ant, *args):
        # todo: instantiate appropriate sprite/image
        s = self.images[ant.kind]
        a = TrackingSprite(s, ant, z_layer=LAYER_TOWER)
        a.use_sub_pixel = True
        self.renderer.add_sprite(a)
        self.entity_sprite[ant] = a

        h = HealthBar(ant, z_layer=LAYER_HEALTH)
        self.assigned_sprites.setdefault(ant, []).append(h)

    def on_ants_died(self, event_type, ants, *args):
        sprites = [self.entity_sprite.get(a, None) for a in ants]
        self.renderer.remove_sprites(sprites)

        for ant in ants:
            self.renderer.remove_sprites(self.assigned_sprites.get(ant, []))

        for a in ants:
            self.entity_sprite.pop(a, None)  # need the default value to not raise KeyError if not present
            self.assigned_sprites.pop(a, None)

    def on_power_loss(self, event_type, building, *args):
        img = self.images["power loss!"]
        spr = TrackingSprite(img, building, z_layer=LAYER_TOWER_POWER_LOSS)
        self.power_sprites[building] = spr
        self.renderer.add_sprite(spr)

    def on_power_restore(self, event_type, building, *args):
        spr = self.power_sprites.get(building, None)
        if spr:
            del self.power_sprites[building]
            self.renderer.remove_sprite(spr)

    def on_not_enough_chips_to_fire(self, event_type, building):
        img = self.images["no chips to fire!"]
        spr = TrackingSprite(img, building, z_layer=LAYER_TOWER_LOW_CHIPS)
        self.chips_sprites[building] = spr
        self.renderer.add_sprite(spr)

    def on_ready_to_fire(self, event_type, building):
        spr = self.chips_sprites.get(building, None)
        if spr:
            del self.chips_sprites[building]
            self.renderer.remove_sprite(spr)

    def on_building_destroyed(self, event_type, building):
        self.on_building_removed(event_type, building)  # is this good enough? yes

    def on_time_indicator_added(self, event_type, indicator, *args):
        spr = IndicatorSprite(indicator, LAYER_INDICATOR)
        self.entity_sprite[indicator] = spr
        self.renderer.add_sprite(spr)

    def on_time_indicator_removed(self, event_type, indicator, *args):
        spr = self.entity_sprite[indicator]
        del self.entity_sprite[indicator]
        self.renderer.remove_sprite(spr)

    def on_building_removed(self, event_type, building, *args):
        spr = self.entity_sprite.get(building, None)
        if spr:
            self.renderer.remove_sprite(spr)
            del self.entity_sprite[building]

        sprites = self.assigned_sprites.get(building, None)
        if sprites:
            self.renderer.remove_sprites(sprites)
            del self.assigned_sprites[building]

    def on_pool_created(self, event_type, pool, *args):
        if pool.kind == KIND_ENERGY_STORAGE_POOL:
            self.info_hud.add("energy pool", "Energy: {0}/{1}", pool.amount, pool.capacity,
                              callback=lambda: (pool.amount, pool.capacity))
        else:
            self.info_hud.add("chips pool", "Crypto Chips: {}", pool.amount, callback=lambda: pool.amount)

    def on_bullets_created(self, event_type, bullets, *args):
        bullet_sprites = [TrackingSprite(self.images[b.kind], b, z_layer=LAYER_BULLET) for b in bullets]
        self.renderer.add_sprites(bullet_sprites)
        for spr in bullet_sprites:
            self.entity_sprite[spr.tracked_entity] = spr

    def on_bullets_destroyed(self, event_type, bullets, *args):
        for b in bullets:
            spr = self.entity_sprite.get(b, None)
            self.renderer.remove_sprite(spr)
            self.entity_sprite.pop(b)

    def on_path_created(self, event_type, path, *args):
        spr = PathDrawer(path, LAYER_PATH)
        self.renderer.add_sprite(spr)

    def on_wave_started(self, event_type, wave_num):
        self.current_wave_num = wave_num

    def on_ant_turned(self, event_type, ant):
        spr = self.entity_sprite.get(ant, None)
        if spr:
            spr.rotation = -ant.angle

    def handle_events(self):
        events = pygame.event.get()
        actions, unmapped = gameplay_event_map.get_actions(events)
        for action, extra in actions:
            if action != ACTION_MOUSE_MOTION:
                logger.debug('action {0}, {1}', action, extra)
            if action == ACTION_TOGGLE_DEBUG_RENDER:
                self._debug_render = not self._debug_render
                self._debug_render_changed()
            elif action == ACTION_QUIT:
                event_dispatcher.fire(EVT_QUIT)
            elif action == ACTION_MOUSE_MOTION:
                pos, rel, buttons = extra
                self.current_mouse_mode.on_mouse_motion(pos, rel, buttons)
                # logger.info("mouse motion: {0}, {1}, {2}", pos, rel, buttons)
            elif action == ACTION_MBUTTON1_DOWN:
                pos = extra
                self.current_mouse_mode.on_mouse_button1_down(pos)
                # logger.info("mouse button up: {0}", pos)
            elif action == ACTION_OTHER_MBUTTON_DOWN:
                pos = extra
                self.current_mouse_mode.on_other_mouse_button_down(pos)

        for event in unmapped:
            if event.type == music_ended_pygame_event:
                logger.info("music ended event!")
                self.music_player.on_music_ended()

    def update_step(self, delta, sim_time, *args):
        self.handle_events()
        self.tweener.update(delta)
        message_display.update()

    def draw(self, screen, do_flip=True, interpolation_factor=1.0):
        # self.renderer.draw(screen, self.cam, default_fill_color, False, interpolation_factor)
        screen.fill(pygame.Color('tan'), self.chipworks_rect)
        screen.fill(pygame.Color('darkgreen'), self.contested_lands_rect)
        screen.fill(pygame.Color('brown4'), self.ant_country_rect)
        self.renderer.draw(screen, self.cam, None, False)
        if self.debug_hud_on:
            self.debug_hud.draw(screen)
        self.hud.draw(screen, self.cam_hud, (255, 255, 255), do_flip)
        self.clock.tick()

    def _debug_render_changed(self):
        if self._debug_render:
            self._debug_sprites = [VectorSprite(Vec(1, 0, 0) * 50, Point(0, 0, 0), (255, 0, 0)),
                                   VectorSprite(Vec(0, 1, 0) * 50, Point(0, 0, 0), (0, 255, 0)),
                                   VectorSprite(Vec(0, 0, 1) * 50, Point(0, 0, 0), (0, 0, 255))]

            self._debug_hud_sprites = [VectorSprite(Vec(1, 0, 0) * 50, Point(0, 0, 0), (255, 0, 0)),
                                       VectorSprite(Vec(0, 1, 0) * 50, Point(0, 0, 0), (0, 255, 0)),
                                       VectorSprite(Vec(0, 0, 1) * 50, Point(0, 0, 0), (0, 0, 255))]

            for ent, spr in self.entity_sprite.items():
                if isinstance(spr, TrackingSprite):
                    radius = getattr(ent, "radius", None)
                    if radius:
                        self._debug_sprites.append(RadiusSprite(ent, LAYER_TOWER + 1, True, ent.radius))

            self.renderer.add_sprites(self._debug_sprites)
            self.hud.add_sprites(self._debug_hud_sprites)
            self.debug_hud_on = True
        else:
            self.renderer.remove_sprites(self._debug_sprites)
            self.hud.remove_sprites(self._debug_hud_sprites)
            self._debug_sprites = []
            self._debug_hud_sprites = []
            self.debug_hud_on = False


class _MouseMode(object):

    def __init__(self, renderer, hud_renderer):
        self.hud_renderer = hud_renderer
        self.renderer = renderer

    def on_mouse_motion(self, pos, rel, buttons):
        raise NotImplementedError("overwrite in sub class!")

    def on_mouse_button1_down(self, pos):
        raise NotImplementedError("overwrite in sub class!")

    def on_other_mouse_button_down(self, pos):
        raise NotImplementedError("overwrite in sub class!")


class SelectionMode(_MouseMode):

    def __init__(self, renderer, hud_renderer):
        _MouseMode.__init__(self, renderer, hud_renderer)
        self._hovered = None

    def on_other_mouse_button_down(self, pos):
        # r click to upgrade
        sprites = self.renderer.get_sprites_at_tuple(pos)
        if sprites:
            # for spr in sprites:
            #     ent = getattr(spr, 'tracked_entity', None)
            #     if ent:
            #         event_dispatcher.fire(ACTION_UPGRADE_BUILDING, ent)
            #         break
            #     else:
            #         logger.debug("can't upgrade sprite {0}", sprites[0])
            hits = []
            self._hit_by_radius(hits, pos, sprites)
            if hits:
                ent = hits[0].tracked_entity
                event_dispatcher.fire(ACTION_UPGRADE_BUILDING, ent)

    def on_mouse_button1_down(self, pos):
        sprites = self.renderer.get_sprites_at_tuple(pos)
        self._on_mouse_button_down(sprites)
        sprites = self.hud_renderer.get_sprites_at_tuple(pos)
        self._on_mouse_button_down(sprites)

    def _on_mouse_button_down(self, sprites):
        if sprites:
            for spr in sprites:
                ent = getattr(spr, 'tracked_entity', None)
                if ent:
                    if hasattr(ent, 'on_click'):
                        ent.on_click()
                        break
                else:
                    logger.debug("clicked on a non clickable sprite {0}", sprites[0])

    def on_mouse_motion(self, pos, rel, buttons):
        hits = []
        sprites = self.renderer.get_sprites_at_tuple(pos)
        if sprites:
            # hits.append(sprites[0])
            self._hit_by_radius(hits, pos, sprites)

        sprites = self.hud_renderer.get_sprites_at_tuple(pos)
        if sprites:
            hits.append(sprites[0])

        if hits:
            # assert len(hits) == 1, "should not have more than 1 hit at once"
            hit = hits[0]
            if self._hovered == hit:
                return
            if self._hovered:
                event_dispatcher.fire(EVT_FOCUS_LOST, self._hovered)
            self._hovered = hit
            event_dispatcher.fire(EVT_FOCUS_GAINED, self._hovered)
        else:
            if self._hovered:
                event_dispatcher.fire(EVT_FOCUS_LOST, self._hovered)
                self._hovered = None

    def _hit_by_radius(self, hits, pos, sprites):
        for spr in sprites:
            ent = getattr(spr, 'tracked_entity', None)
            if ent:
                radius = getattr(ent, 'radius', None)
                if radius:
                    # HACK: this only works because we set the center of the entity to the bottom part of the image such
                    #       that the circle is within the image
                    mbx, mby = spr.rect.midbottom
                    mby -= radius
                    if radius and (mbx - pos[0]) ** 2 + (mby - pos[1]) ** 2 < radius ** 2:
                        hits.append(spr)
                        break


class PlacingMode(_MouseMode):

    def __init__(self, renderer, hud_renderer, placing_sprite, cam, space, images):
        _MouseMode.__init__(self, renderer, hud_renderer)
        self.images = images
        self.space = space
        self.cam = cam
        self._building_type = None
        self.placing_sprite = placing_sprite
        renderer.add_sprite(placing_sprite)
        self.placing_sprite.visible = False

    def set_selection(self, building_type):
        self._building_type = building_type

    def on_other_mouse_button_down(self, pos):
        event_dispatcher.fire(EVT_UI_SWITCHED_TO_SELECTION_MOUSE_MODE)
        self.placing_sprite.visible = False

    def on_mouse_button1_down(self, pos):
        sprites = self.hud_renderer.get_sprites_at_tuple(pos)
        if sprites:
            ent = getattr(sprites[0], 'tracked_entity', None)
            if ent:
                if hasattr(ent, 'on_click'):
                    ent.on_click()
            else:
                logger.debug("clicked on a non clickable sprite {0}", sprites[0])
            return

        # place building at position
        world_pos = self.cam.screen_to_world(Point(pos[0], pos[1]))
        event_dispatcher.fire(EVT_BUILDING_PLACED, self._building_type, world_pos)

    def on_mouse_motion(self, pos, rel, buttons):
        sprites = self.renderer.get_sprites_at_tuple(pos)
        if sprites:
            print(sprites[0])
        self.placing_sprite.old_position.copy_values(self.placing_sprite.position)
        x, y = pos
        self._update_placing_sprite_position(x, y)

    def update_placing_sprite(self, building_type, *args):
        self._building_type = building_type
        img = self.images[(building_type.kind, building_type.level)]
        self.placing_sprite.set_image(img)
        self.placing_sprite.alpha = 128
        iw, ih = img.get_size()
        self.placing_sprite.anchor = Vec(0, ih // 2 - building_type.radius)
        x, y = pygame.mouse.get_pos()
        self._update_placing_sprite_position(x, y)

    def _update_placing_sprite_position(self, x, y):
        if y > main_screen_rect[3]:
            y = main_screen_rect[3]
        w_pos = self.cam.screen_to_world(Point(x, y))
        self.placing_sprite.position.x = w_pos.x
        self.placing_sprite.position.y = w_pos.y
        self.placing_sprite.old_position.copy_values(self.placing_sprite.position)
        self.placing_sprite.visible = True


class SfxHandler(object):
    def __init__(self):
        self.sfx_player = sfx.SoundPlayer(1.0)
        self.sfx_player.setup_channels(settings.MIXER_NUM_CHANNELS, settings.MIXER_RESERVED_CHANNELS)
        self.sfx_player.load(resource_sound.sfx_data)

        event_dispatcher.register_event_type(EVT_SOUND_STARTED)
        event_dispatcher.add_listener(EVT_SOUND_STARTED, self.sfx_player.handle_message)

        event_dispatcher.add_listener(EVT_TOWER_CREATED, self._on_building_up)
        event_dispatcher.add_listener(EVT_UPGRADE_CENTER_CREATED, self._on_building_up)
        event_dispatcher.add_listener(EVT_POWER_STATION_CREATED, self._on_building_up)
        event_dispatcher.add_listener(EVT_SCIENCE_CENTER_CREATED, self._on_building_up)
        event_dispatcher.add_listener(EVT_CHIPS_FACTORY_CREATED, self._on_building_up)
        event_dispatcher.add_listener(EVT_BATTERY_CREATED, self._on_building_up)

        event_dispatcher.add_listener(EVT_TOWER_DESTROYED, self._on_building_down)
        event_dispatcher.add_listener(EVT_UPGRADE_CENTER_DESTROYED, self._on_building_down)
        event_dispatcher.add_listener(EVT_POWER_STATION_DESTROYED, self._on_building_down)
        event_dispatcher.add_listener(EVT_SCIENCE_CENTER_DESTROYED, self._on_building_down)
        event_dispatcher.add_listener(EVT_CHIPS_FACTORY_DESTROYED, self._on_building_down)
        event_dispatcher.add_listener(EVT_BATTERY_DESTROYED, self._on_building_down)

        event_dispatcher.add_listener(EVT_BUILDING_PLACED, self._on_building_placed)
        event_dispatcher.add_listener(EVT_BUILDING_HIT, self._on_building_hit)

        event_dispatcher.add_listener(EVT_BULLETS_CREATED, self._on_bullet_created)

        event_dispatcher.add_listener(EVT_OUT_OF_POWER, self._on_no_power)
        event_dispatcher.add_listener(EVT_NO_CHIPS_TO_FIRE, self._on_no_chips)

        event_dispatcher.add_listener(EVT_POWER_RESTORED, self._on_power_restored)
        event_dispatcher.add_listener(EVT_READY_TO_FIRE, self._on_chips_restored)

    def _on_building_up(self, event_type, *args):
        event_dispatcher.fire(EVT_SOUND_STARTED, resource_sound.SFX_BUILDING_CREATED, None)

    def _on_building_down(self, event_type, *args):
        event_dispatcher.fire(EVT_SOUND_STARTED, resource_sound.SFX_BUILDING_DESTROYED, None)

    def _on_building_placed(self, event_type, *args):
        event_dispatcher.fire(EVT_SOUND_STARTED, resource_sound.SFX_BUILDING_PLACED, None)

    def _on_building_hit(self, event_type, *args):
        event_dispatcher.fire(EVT_SOUND_STARTED, random.choice(resource_sound.munches), None)

    def _on_bullet_created(self, event_type, *args):
        event_dispatcher.fire(EVT_SOUND_STARTED, resource_sound.SFX_BULLET_CREATED, None)

    def _on_no_power(self, event_type, *args):
        event_dispatcher.fire(EVT_SOUND_STARTED, resource_sound.SFX_NO_POWER, 'mutex')

    def _on_no_chips(self, event_type, *args):
        event_dispatcher.fire(EVT_SOUND_STARTED, resource_sound.SFX_NO_CHIPS, 'mutex')

    def _on_power_restored(self, event_type, *args):
        self.sfx_player.fadeout(resource_sound.SFX_NO_POWER, 250)

    def _on_chips_restored(self, event_type, *args):
        self.sfx_player.fadeout(resource_sound.SFX_NO_CHIPS, 250)


logger.debug("imported")
