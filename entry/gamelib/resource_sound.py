# -*- coding: utf-8 -*-
import logging

from gamelib import settings
from gamelib.sfx import MusicData, SoundData
from pyknic.generators import IdGenerator

logger = logging.getLogger(__name__)

_gen_id = IdGenerator(1).next

SFX_BUILDING_CREATED = _gen_id()
SFX_BUILDING_DESTROYED = _gen_id()
SFX_BUILDING_PLACED = _gen_id()
SFX_BULLET_CREATED = _gen_id()
SFX_NO_POWER = _gen_id()
SFX_NO_CHIPS = _gen_id()

SFX_MUNCH_1 = _gen_id()
SFX_MUNCH_2 = _gen_id()
SFX_MUNCH_3 = _gen_id()
SFX_MUNCH_4 = _gen_id()
SFX_MUNCH_5 = _gen_id()
SFX_MUNCH_6 = _gen_id()

munches = (
    SFX_MUNCH_1,
    SFX_MUNCH_2,
    SFX_MUNCH_3,
    SFX_MUNCH_4,
    SFX_MUNCH_5,
    SFX_MUNCH_6,
)

SONG_1 = _gen_id()
SONG_2 = _gen_id()
SONG_3 = _gen_id()
SONG_4 = _gen_id()

SONG_WIN = _gen_id()
SONG_LOSE = _gen_id()


def _sp(filename):
    """extend sound path to filename"""
    return '{}/{}'.format(settings.sfx_path, filename)


sfx_data = {
    SFX_BUILDING_CREATED: SoundData(_sp('building_up.ogg'), settings.sfx_volume * settings.master_volume,
                                    reserved_channel_id=settings.channel_building),
    SFX_BUILDING_DESTROYED: SoundData(_sp('building_down.ogg'), settings.sfx_volume * settings.master_volume,
                                      reserved_channel_id=settings.channel_building),
    SFX_BUILDING_PLACED: SoundData(_sp('building_placed.ogg'), settings.sfx_volume * settings.master_volume),
    SFX_BULLET_CREATED: SoundData(_sp('single_shot.ogg'), settings.sfx_volume * settings.master_volume,
                                  reserved_channel_id=settings.channel_bullet),
    SFX_NO_POWER: SoundData(_sp('no_power.ogg'), settings.sfx_volume * settings.master_volume,
                            reserved_channel_id=settings.channel_power),
    SFX_NO_CHIPS: SoundData(_sp('no_chips.ogg'), settings.sfx_volume * settings.master_volume,
                            reserved_channel_id=settings.channel_chips),

    SFX_MUNCH_1: SoundData(_sp('munch1.ogg'), settings.sfx_volume * settings.master_volume,
                           reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_2: SoundData(_sp('munch2.ogg'), settings.sfx_volume * settings.master_volume,
                           reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_3: SoundData(_sp('munch3.ogg'), settings.sfx_volume * settings.master_volume,
                           reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_4: SoundData(_sp('munch4.ogg'), settings.sfx_volume * settings.master_volume,
                           reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_5: SoundData(_sp('munch5.ogg'), settings.sfx_volume * settings.master_volume,
                           reserved_channel_id=settings.channel_ants_speaking),
    SFX_MUNCH_6: SoundData(_sp('munch6.ogg'), settings.sfx_volume * settings.master_volume,
                           reserved_channel_id=settings.channel_ants_speaking),
}


def _mp(filename):
    """extend music path to filename"""
    return '{}/{}'.format(settings.music_path, filename)


songs = [
    MusicData(_mp('Wolf Arm - The Singularity.ogg'), settings.music_volume * settings.master_volume),
    MusicData(_mp('Retrology - Hyperspace.ogg'), settings.music_volume * settings.master_volume),
    MusicData(_mp('CYBERTHING! - Fight to be Alive.ogg'), settings.music_volume * settings.master_volume),
    MusicData(_mp('JEREMIAH KANE - 2500 RACING.ogg'), settings.music_volume * settings.master_volume),
]

song_win = MusicData(_mp('Clash Defiant.ogg'), settings.music_volume * settings.master_volume)
song_lose = MusicData(_mp('Crypto.ogg'), settings.music_volume * settings.master_volume)
