# -*- coding: utf-8 -*-
import logging

import pygame

from gamelib import pygametext
from gamelib import settings
from pyknic.pyknic_pygame.spritesystem import Sprite
from pyknic.tweening import ease_out_quad, ease_in_quad
from pyknic.mathematics import Point2


logger = logging.getLogger(__name__)


class SystemMessages(object):
    normal_queue = []
    fast_queue = []
    current_normal_sprite = None
    current_fast_sprite = None
    renderer = None
    tweener = None
    game_over = False


class SystemMessage(object):
    def __init__(self, text):
        pass

    def _on_expired(self, event_type):
        pass


def init_system(renderer, tweener):
    logger.debug('initializing system messages')
    del SystemMessages.normal_queue[:]
    del SystemMessages.fast_queue[:]
    SystemMessages.renderer = renderer
    SystemMessages.tweener = tweener
    SystemMessages.game_over = False


def add_system_message(event_type, text, fast_queue=False):
    assert event_type == settings.EVT_NEW_SYSTEM_MESSAGE, 'unexpected event_type: {}'.format(event_type)
    if SystemMessages.game_over:
        return
    if text and isinstance(text, str):
        if fast_queue:
            logger.debug('add fast message: "{}"', text)
            SystemMessages.fast_queue.append(text)
        else:
            logger.debug('add normal message: "{}"', text)
            SystemMessages.normal_queue.append(text)
            # TODO: HACK :D
            if text in settings.game_over_messages:
                SystemMessages.game_over = True
    else:
        logger.warning('non-text message ignored: {}', text)

_layer = 0

def update(*args):
    def _cb_at_right(*args):
        spr, wait = args[0:2]
        logger.debug('message_display: _cb_at_right: text="{}"', spr.text)
        if spr is SystemMessages.current_normal_sprite:
            SystemMessages.current_normal_sprite = None
        SystemMessages.renderer.remove_sprite(spr)

    def _cb_at_pause(*args):
        spr, wait = args[0:2]
        logger.debug('message_display: _cb_at_pause: text="{}"', spr.text)
        if spr.text not in settings.game_over_messages:
            SystemMessages.tweener.create_tween_by_end(
                spr.position, 'x', screen_rect.centerx, screen_rect.right, 0.2, ease_in_quad,
                cb_end=_cb_at_right, cb_args=[spr, wait])

    def _cb_at_centerx(*args):
        spr, wait = args[0:2]
        logger.debug('message_display: _cb_at_centerx: text="{}"', spr.text)
        x = spr.position.x
        SystemMessages.tweener.create_tween_by_end(
            spr.position, 'x', x, x, wait, cb_end=_cb_at_pause, cb_args=[spr, wait])

    if SystemMessages.renderer is None or SystemMessages.tweener is None:
        # logger.debug('message_display: return 2')
        return

    if SystemMessages.normal_queue and SystemMessages.current_normal_sprite is None:
        logging.debug('message_display: normal_queue: {}', SystemMessages.normal_queue)
        text = SystemMessages.normal_queue.pop(0)
        logger.debug('message_display: creating sprite w/ tweener: text="{}"', text)
        text_img = pygametext.getsurf(text, **settings.font_themes['intro'])
        screen_rect = pygame.display.get_surface().get_rect()
        w = text_img.get_width()
        x, y = -w // 2, settings.system_message_y
        spr = Sprite(text_img, Point2(x, y), z_layer=settings.LAYER_SYSTEM_MESSAGE)
        spr.text = text
        SystemMessages.tweener.create_tween_by_end(
            spr.position, 'x', spr.position.x, screen_rect.centerx, 0.3, ease_out_quad,
            cb_end=_cb_at_centerx, cb_args=[spr, settings.normal_message_wait])
        SystemMessages.current_normal_sprite = spr
        SystemMessages.renderer.add_sprite(spr)

    if SystemMessages.fast_queue :#and SystemMessages.current_fast_sprite is None:
        logging.debug('message_display: fast_queue: {}', SystemMessages.fast_queue)
        spr = SystemMessages.current_fast_sprite
        if spr:
            SystemMessages.tweener.remove_tween(spr.tween)
            SystemMessages.renderer.remove_sprite(spr)
        text = SystemMessages.fast_queue.pop(0)
        logger.debug('message_display: creating sprite w/ tweener: text="{}"', text)
        theme = settings.font_themes['intro'].copy()
        theme.update(fontsize=42)
        text_img = pygametext.getsurf(text, **theme)
        screen_rect = pygame.display.get_surface().get_rect()
        w = text_img.get_width()
        x, y = -w // 2, settings.system_message_y
        y += settings.font_themes['intro']['fontsize'] + 3
        global _layer
        spr = Sprite(text_img, Point2(x, y), z_layer=settings.LAYER_SYSTEM_MESSAGE + _layer)
        _layer += 1  # make sure newest ist show at top
        spr.text = text
        spr.tween = SystemMessages.tweener.create_tween_by_end(
            spr.position, 'x', spr.position.x, screen_rect.centerx, 0.3, ease_out_quad,
            cb_end=_cb_at_centerx, cb_args=[spr, settings.short_message_wait])
        SystemMessages.current_fast_sprite = spr
        SystemMessages.renderer.add_sprite(spr)


def clear():
    renderer = SystemMessages.renderer
    del SystemMessages.normal_queue[:]
    del SystemMessages.fast_queue[:]
    if renderer:
        if SystemMessages.current_normal_sprite:
            renderer.remove_sprite(SystemMessages.current_normal_sprite)
            SystemMessages.current_normal_sprite = None
        # if SystemMessages.current_fast_sprite:
        #     renderer.remove_sprite(SystemMessages.current_fast_sprite)
        #     SystemMessages.current_fast_sprite = None
