# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'ants.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Ant entities module.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging
import random

import pygame

from gamelib.eventing import event_dispatcher
from gamelib.settings import KIND_ANT, KIND_BATTERY, KIND_TOWER_FAN, KIND_TOWER_MACHINE_GUN, KIND_TOWER_NORMAL, \
    KIND_TOWER_SNIPER, KIND_E, KIND_F, KIND_CHIPS_FACTORY, KIND_POWER, KIND_SCIENCE, KIND_UPGRADE, \
    ant_max_search_radius, ant_turn_angle, ant_turn_angle_step, EVT_ANT_TURNED
from pyknic.ai.statemachines import StateDrivenAgentBase, BaseState

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class AntTemplate(object):
    speed = 20  # 50
    size = 1.0
    radius = 10
    health = 4  # 100
    height = 10
    attack_interval = 0.5
    attack_damage = 10  # orig: 1


class AntBase(BaseState):

    @staticmethod
    def on_timer(owner):
        pass


class FollowPath(AntBase):

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if owner.target.position.get_distance_sq(owner.position) < 3 * 3:
            # reached target
            if owner.current_path_node_idx < len(owner.path) - 1:
                owner.current_path_node_idx += 1
                owner.target = owner.path[owner.current_path_node_idx]
            else:
                # reached path end
                owner.state_machine.switch_state(FindBuilding)

        owner.direction = (owner.target.position - owner.position).normalized
        owner.update_angle()

        owner.position += owner.speed * owner.direction * dt


class FindBuilding(AntBase):

    @staticmethod
    def enter(owner):
        owner.search_radius = owner.template.radius * 3

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if owner.search_radius > ant_max_search_radius:
            owner.state_machine.switch_state(Wander)

        r = pygame.Rect(0, 0, owner.search_radius * 2, owner.search_radius * 2)
        r.center = owner.position.as_xy_tuple(int)
        others = owner.space.get_in_rect(*r)
        if others:
            target_types = (KIND_BATTERY, KIND_TOWER_FAN, KIND_TOWER_MACHINE_GUN, KIND_TOWER_NORMAL, KIND_TOWER_SNIPER,
                            KIND_E, KIND_F, KIND_CHIPS_FACTORY, KIND_POWER, KIND_SCIENCE, KIND_UPGRADE)
            targets = [o for o in others if o.kind in target_types]
            if targets:
                owner.target = targets[0]  # choose randomly one
                owner.state_machine.switch_state(MoveToTarget)
                return
        owner.search_radius *= 2


class MoveToTarget(AntBase):

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        if owner.target.health <= 0:
            owner.state_machine.switch_state(FindBuilding)
            owner.target = None
            return

        if owner.target.position.get_distance_sq(owner.position) < (owner.radius + owner.target.radius) ** 2:
            # reached target
            owner.state_machine.switch_state(Attack)
            return

        owner.direction = (owner.target.position - owner.position).normalized
        owner.update_angle()

        owner.position += owner.speed * owner.direction * dt


class Attack(AntBase):

    @staticmethod
    def enter(owner):
        owner.timer.interval = owner.template.attack_interval
        owner.timer.start()

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner):
        if owner.target.health <= 0:
            owner.state_machine.switch_state(FindBuilding)
            return

        owner.target.on_damage(owner.template.attack_damage)


class Wander(AntBase):

    @staticmethod
    def enter(owner):
        owner.timer.interval = owner.template.attack_interval
        owner.timer.start()

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner):
        owner.direction.rotate(random.choice(range(-ant_turn_angle, ant_turn_angle, ant_turn_angle_step)))
        owner.update_angle()
        if random.random() < 0.2:  # 1/5 chance
            owner.state_machine.switch_state(FindBuilding)

    @staticmethod
    def update(owner, dt, sim_t, *args, **kwargs):
        owner.position += owner.speed * owner.direction * dt


class Dead(AntBase):
    pass


class Ant(StateDrivenAgentBase):
    kind = KIND_ANT

    def __init__(self, position, path, template, timer, space, initial_state=FollowPath):
        self.search_radius = 0  # sill be set in enter method of FindBuilding
        self.space = space
        if timer:
            self.timer = timer
            self.timer.repeat = True
            self.timer.event_elapsed += self.on_timer
        self.path = path
        self.target = path[0]
        self.current_path_node_idx = 0
        self.position = position
        self.direction = (self.target.position - self.position).normalized
        self.speed = template.speed
        self.radius = template.radius
        StateDrivenAgentBase.__init__(self, initial_state)
        self.full_health = template.health
        self.health = self.full_health
        self.template = template
        self.angle = self.direction.angle

    def update_angle(self):
        if self.angle != self.direction.angle:
            self.angle = self.direction.angle
            event_dispatcher.fire(EVT_ANT_TURNED, self)

    def is_alive(self):
        return self.health > 0

    def hurt(self, hit_points):
        if self.health <= 0:
            return None  # already dead, cant die twice

        self.health -= hit_points
        if self.health <= 0:
            self.state_machine.switch_state(Dead)  # need this transition to stop all timers
            return self
        return None

    def on_timer(self, *args):
        self.state_machine.current_state.on_timer(self)


class TestAnt(Ant):

    def __init__(self, position):
        Ant.__init__(self, position, [Node(position)], AntTemplate, None, None, Dead)
        self.health = 100000

class Node(object):

    def __init__(self, position):
        self.position = position


logger.debug("imported")
