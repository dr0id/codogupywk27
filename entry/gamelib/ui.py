# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'ui.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
UI elements.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

import pygame

from gamelib.eventing import event_dispatcher
from gamelib.settings import EVT_UNLOCKED, health_bar_back_color, health_bar_color, health_bar_size, \
    level_indicator_color, level_indicator_y_offset, level_indicator_radius, level_indicator_step_size, max_levels, \
    level_indicator_x_offset, build_bar_size, build_bar_back_color, build_bar_color, EVT_TYPE_UNLOCKED, \
    level_indicator_back_color, use_level_indicator_back_ground
from pyknic.mathematics import Point3, Vec3
from pyknic.pyknic_pygame.spritesystem import Sprite

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class TrackingSprite(Sprite):

    def __init__(self, image, tracked_entity, anchor=None, z_layer=None, parallax_factors=None, do_init=True,
                 name='_NO_NAME_'):
        Sprite.__init__(self, image, tracked_entity.position, anchor, z_layer, parallax_factors, do_init,
                        name)
        self.tracked_entity = tracked_entity


class LockSprite(TrackingSprite):
    def __init__(self, image, tracked_entity, anchor=None, z_layer=None, parallax_factors=None, do_init=True,
                 name='_NO_NAME_'):
        TrackingSprite.__init__(self, image, tracked_entity, anchor, z_layer, parallax_factors, do_init, name)
        self.alpha = 160
        event_dispatcher.add_listener(EVT_UNLOCKED, self._on_unlock)

    def _on_unlock(self, event_type, button, unlock_type):
        if self.tracked_entity == button:
            event_dispatcher.remove_listener(self._on_unlock)
            self.visible = False


class Button(object):

    def __init__(self, on_click_callback, pos):
        self.position = pos
        self._on_click_callback = on_click_callback

    def on_click(self, *args):
        self._on_click_callback(*args)


class LockButton(Button):

    def __init__(self, on_click_callback, unlock_type, pos=None):
        pos = Point3(0, 0) if pos is None else pos
        Button.__init__(self, on_click_callback, pos)
        self._unlock_type = unlock_type
        self._locked = True
        event_dispatcher.add_listener(EVT_TYPE_UNLOCKED, self._on_event)

    def on_click(self, *args):
        if self._locked:
            return
        Button.on_click(self, self._unlock_type)

    def _on_event(self, event_type, building_type, *args):
        if self._unlock_type == building_type:
            self._locked = False
            event_dispatcher.fire(EVT_UNLOCKED, self, self._unlock_type)


class FireRangeSprite(TrackingSprite):
    draw_special = True
    dirty_update = False

    def __init__(self, tracked_entity, z_layer, visible, color=(0, 255, 0)):
        TrackingSprite.__init__(self, None, tracked_entity, z_layer=z_layer)
        self.color = color
        self.visible = visible

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        screen_pos = cam.world_to_screen(self.tracked_entity.position).as_xy_tuple(int)
        # pygame.draw.circle(surf, self.color, screen_pos, 3)
        return pygame.draw.circle(surf, self.color, screen_pos, self.tracked_entity.template.fire_range, 1)


class RadiusSprite(TrackingSprite):
    draw_special = True
    dirty_update = False

    def __init__(self, tracked_entity, z_layer, visible, radius, color=(0, 255, 0)):
        TrackingSprite.__init__(self, None, tracked_entity, z_layer=z_layer)
        self.color = color
        self.visible = visible
        self.radius = radius

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        screen_pos = cam.world_to_screen(self.tracked_entity.position).as_xy_tuple(int)
        pygame.draw.circle(surf, self.color, screen_pos, 3)
        return pygame.draw.circle(surf, self.color, screen_pos, self.radius, 1)


class IndicatorSprite(TrackingSprite):
    draw_special = True
    dirty_update = False

    def __init__(self, indicator, z_layer):
        TrackingSprite.__init__(self, None, indicator, z_layer=z_layer)

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        screen_pos = cam.world_to_screen(self.tracked_entity.position).as_xy_tuple(int)

        back_rect = pygame.Rect(screen_pos, build_bar_size)
        back_rect.center = screen_pos
        pygame.draw.rect(surf, build_bar_back_color, back_rect, 0)
        health_rect = pygame.Rect(screen_pos,
                                  (int(build_bar_size[0] * self.tracked_entity.completed), build_bar_size[1]))
        health_rect.topleft = back_rect.topleft
        pygame.draw.rect(surf, build_bar_color, health_rect, 0)
        return back_rect


class HealthBar(TrackingSprite):
    draw_special = True
    dirty_update = False

    def __init__(self, entity, z_layer):
        TrackingSprite.__init__(self, None, entity, z_layer=z_layer)

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        screen_pos = cam.world_to_screen(
            self.tracked_entity.position + Vec3(0, -self.tracked_entity.template.height - health_bar_size[1])).as_xy_tuple(int)

        w = health_bar_size[0]
        w = int(self.tracked_entity.full_health / 100.0 * w)
        if w < health_bar_size[0]:
            w = health_bar_size[0]
        back_rect = pygame.Rect(screen_pos, (w, health_bar_size[1]))
        back_rect.center = screen_pos
        pygame.draw.rect(surf, health_bar_back_color, back_rect, 0)

        ww = int(w * self.tracked_entity.health / self.tracked_entity.full_health)
        health_rect = pygame.Rect(back_rect.topleft, (ww, health_bar_size[1]))
        pygame.draw.rect(surf, health_bar_color, health_rect, 0)
        return back_rect


class LevelIndicator(TrackingSprite):
    draw_special = True
    dirty_update = False

    def __init__(self, entity, z_layer):
        TrackingSprite.__init__(self, None, entity, z_layer=z_layer)

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        sx, sy = cam.world_to_screen(
            self.tracked_entity.position + Vec3(0, -self.tracked_entity.template.height)).as_xy_tuple(
            int)

        step = level_indicator_step_size
        radius = level_indicator_radius
        sy += level_indicator_y_offset

        if use_level_indicator_back_ground:
            back_rect = pygame.Rect((0, 0) , health_bar_size)
            back_rect.center =  (sx, sy)
            pygame.draw.rect(surf, level_indicator_back_color, back_rect)

        sx += level_indicator_x_offset  # half health bar
        r = pygame.Rect(sx, sy, 0, 0)

        for i in range(max_levels):
            rr = pygame.draw.circle(surf, level_indicator_color, (sx + i * step, sy), radius, 1)
            r.union_ip(rr)
            if self.tracked_entity.level > i:
                pygame.draw.circle(surf, level_indicator_color, (sx + i * step, sy), radius, 0)
        return r


# class ValueTextSprite(TextSprite):
#     draw_special = True
#     dirty_update = True
#
#     def __init__(self, tracked_entity, attribute_name, position, z_layer):
#         text = str(getattr(tracked_entity, attribute_name))
#         TextSprite.__init__(self, text, position, z_layer=z_layer)
#         self.tracked_entity = tracked_entity
#         self.attribute_name = attribute_name
#
#     def draw(self, surf, cam, renderer, interpolation_factor=1.0):
#         sx, sy = cam.world_to_screen(self.position).as_xy_tuple(int)
#         new_text = str(getattr(self.tracked_entity, self.attribute_name))
#         if new_text != self._text:
#             self.text = new_text
#         ox, oy = self.offset.as_xy_tuple(int)
#         return surf.blit(self.image, (sx + ox, sy + oy))


class PathDrawer(Sprite):
    draw_special = True
    dirty_update = False

    def __init__(self, tracked_entity, z_layer):
        Sprite.__init__(self, None, Point3(0, 0), z_layer=z_layer)
        self.tracked_entity = tracked_entity

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        path = self.tracked_entity
        node0 = node1 = path[0]
        for node1 in path[1:]:
            p0 = node0.position.as_xy_tuple(int)
            p1 = node1.position.as_xy_tuple(int)
            pygame.draw.line(surf, pygame.Color('brown'), p0, p1, 6)
            # pygame.draw.circle(surf, pygame.Color('red'), p0, 2)
            node0 = node1
        # pygame.draw.circle(surf, pygame.Color('red'), p1, 2)
        return pygame.Rect(0, 0, 0, 0)  # hack: should calculate the actual dirty area, but its not used so fake it


class HudSprite(Sprite):
    draw_special = True
    dirty_update = False

    def __init__(self, hud, position, z_layer):
        Sprite.__init__(self, None, position, z_layer=z_layer)
        self.hud = hud

    def draw(self, surf, cam, renderer, interpolation_factor=1.0):
        screen_pos = cam.world_to_screen(self.position)
        self.hud.x, self.hud.y = screen_pos.as_xy_tuple(int)
        self.hud.update()
        return self.hud.draw(surf)


logger.debug("imported")
