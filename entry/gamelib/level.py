# -*- coding: utf-8 -*-

import logging

import pygame
import pytmxloader
from gamelib.ants import Node

from pyknic.mathematics import Point3 as Point, Vec3 as Vec, sign
from pyknic.pyknic_pygame.spritesystem import Sprite, TextSprite
from pytmxloader import TILE_LAYER_TYPE


logger = logging.getLogger(__name__)


class Level(object):
    def __init__(self):
        self.map_file_name = None
        self.animations = {}  # {anim_id: [animation]}
        self.ant_paths = []   # [[p1, p2, ...], [p1, p2, ..], ...]
        self.height = 0

    def load_map(self, name, scheduler):
        img_cache = {}
        sprites = []
        resources = {}  # {kind: resource}

        logger.info(">>> LOADING MAP {0}", name)

        self.map_file_name = name
        map_info = pytmxloader.load_map_from_file_path(name)
        self.height = map_info.properties['tileheight'] * map_info.properties['height']
        for idx, layer in enumerate(map_info.layers):
            if not layer.visible:
                continue
            if layer.layer_type == TILE_LAYER_TYPE:
                self._load_tile_layer(idx, img_cache, layer, scheduler, sprites, resources)
            elif layer.layer_type == pytmxloader.OBJECT_LAYER_TYPE:
                self._load_object_layer(idx, img_cache, layer, scheduler, sprites, resources)

        # TODO: anims
        # self._validate_animation_load(settings.ANIM_0_NORMAL)

        logger.info(">>> LOADING MAP {0} DONE", name)
        return sprites

    def _load_tile_layer(self, idx, img_cache, layer, scheduler, sprites, resources):
        logger.debug("layer {0}", layer.name)
        for tile_y in range(layer.height):
            logger.debug("line {0}", tile_y)
            for tile_x in range(layer.width):
                pygame.event.pump()  # pump the pygame event queue to avoid freezing the window

                # TODO: create tiles
                pass

    def _load_object_layer(self, idx, img_cache, layer, scheduler, sprites, resources):
        for ot in layer.tiles:
            ti = ot.tile_info
            kind = ti.properties.get("kind", None)

            if ti.animation:
                # TODO: create animated sprite
                pass
            else:
                # TODO: create sprite from object
                pass

            # TODO: create entity
            if kind == "tower_a":
                pass
            elif kind == "tower_b":
                pass
            elif kind == "tower_c":
                pass
            elif kind == "tower_d":
                pass
            elif kind == "tower_e":
                pass
            elif kind == "tower_f":
                pass

            elif kind == "upgrade_center":
                pass

            elif kind == "power_station":
                pass

            elif kind == "science_center":
                pass

            elif kind == "chips_factory":
                pass

            elif kind == "battery":
                pass

        # TODO: create entity from rectangle
        for ot in layer.rectangles:
            kind = ot.properties.get("kind", None)
            r = pygame.Rect(ot.x, ot.y, ot.width, ot.height)
            spr = None

            if kind == "start":
                pass
            else:
                raise Exception("unexpected kind={} in object layer rectangles layer={} name='{}' id={}".format(
                    kind, layer.name, ot.name, ot.id))

            if spr is not None:
                text = ot.properties.get("text", None)
                x, y, w, h = ot.pixel_rect
                text_pos = spr.position + Vec(0, h / 2 + 5)
                self._create_text_sprite(sprites, text, text_pos)

        for ot in layer.poly_lines:
            kind = ot.properties.get("kind", None)
            ant_path = []
            for p in ot.pixel_points:
                if kind == "path":
                    ant_path.append(Node(Point(*p)))
                else:
                    raise Exception("unexpected kind={} in object layer poly lines layer={} name='{}' id={}".format(
                        kind, layer.name, ot.name, ot.id))
            if ant_path:
                logger.debug('ant_path loaded: {}', ant_path)
                self.ant_paths.append(ant_path)

    @staticmethod
    def _create_text_sprite(self, sprites, text, text_pos):
        if text is not None:
            ts = TextSprite(text, text_pos, z_layer=99, anchor="midtop")
            ts.name = str(id(ts))
            sprites.append(ts)

    @staticmethod
    def _get_cached_image(self, img_cache, ti):
        key = (ti.tileset.image_path_rel_to_cwd, ti.spritesheet_x, ti.spritesheet_y, ti.flip_x, ti.flip_y, ti.angle)
        img = img_cache.get(key, None)
        if img is None:
            spritesheet = img_cache.get(ti.tileset.image_path_rel_to_cwd, None)
            if spritesheet is None:
                # noinspection PyUnresolvedReferences
                spritesheet = pygame.image.load(ti.tileset.image_path_rel_to_cwd).convert_alpha()
                img_cache[ti.tileset.image_path_rel_to_cwd] = spritesheet
            area = pygame.Rect(ti.spritesheet_x, ti.spritesheet_y, ti.tileset.tile_width, ti.tileset.tile_height)
            img = pygame.Surface(area.size, spritesheet.get_flags(), spritesheet)
            img.blit(spritesheet, (0, 0), area)
            img = pygame.transform.flip(img, ti.flip_x, ti.flip_y)
            if ti.angle != 0:
                img = pygame.transform.rotate(img, ti.angle)
            # ip = ti.tileset.image_path_rel_to_cwd
            # if "space1_0_4x_smooth.png" in ip:
            #     img = img.convert()

            # if PIXEL_PPU != 1.0:
            #     if PIXEL_PPU == 2.0:
            #         img = pygame.transform.scale2x(img)
            #     else:
            #         _w, _h = img.get_size()
            #         img = pygame.transform.smoothscale(img, (int(_w * PIXEL_PPU), int(_h * PIXEL_PPU)))

            img = img.convert_alpha()
            img_cache[key] = img
        return img

    def _load_animation_sprite(self, layer, img_cache, position, ti, scheduler):
        frames = []
        fps = 1.0
        for af in ti.animation:
            img = self._get_cached_image(img_cache, af.tile_info)
            fps = 1.0 / (af.duration / 1000.0)
            frames.append(img)
        spr = SpriteAnimation(position, layer, frames, fps, scheduler)
        return spr

    def _load_special_animation_sprite(self, layer, img_cache, position, ti, scheduler, flip_y=False):
        frames = []
        fps = 1.0
        for af in ti.animation:
            img = self._get_cached_image(img_cache, af.tile_info)
            if flip_y:
                img = pygame.transform.flip(img, False, True)
            fps = 1.0 / (af.duration / 1000.0)
            frames.append(img)
        spr = SpecialAnimation(position, LAYER_HERO, frames, fps, scheduler)
        return spr

    def _validate_animation_load(self, anim_id):
        if self.animations.get(anim_id, None) is None:
            raise Exception("missing anim id " + anim_id)
        if len(self.animations[anim_id]) < 2:
            raise Exception("no/less animations loaded (anim id {0}) should at leas be 3 but {1}".format(anim_id, len(
                self.animations[anim_id])))
