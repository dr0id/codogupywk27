# -*- coding: utf-8 -*-
#
# New BSD license
#
# Copyright (c) DR0ID
# This file 'buildings.py' is part of codogupywk27
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the <organization> nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL DR0ID BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Building entities.

.. versionchanged:: 0.0.0.0
    initial version

"""
from __future__ import print_function

import logging

from gamelib.eventing import event_dispatcher
from gamelib.settings import KIND_TIME_INDICATOR, EVT_TOWER_CREATED, EVT_UPGRADE_CENTER_CREATED, \
    EVT_POWER_STATION_CREATED, EVT_SCIENCE_CENTER_CREATED, EVT_CHIPS_FACTORY_CREATED, EVT_BATTERY_CREATED, \
    EVT_TIME_INDICATOR_REMOVED, EVT_TOWER_DESTROYED, EVT_UPGRADE_CENTER_DESTROYED, EVT_POWER_STATION_DESTROYED, \
    EVT_SCIENCE_CENTER_DESTROYED, EVT_CHIPS_FACTORY_DESTROYED, EVT_BATTERY_DESTROYED, EVT_QUIT, EVT_BUILDING_UPGRADED, \
    EVT_OUT_OF_POWER, EVT_POWER_RESTORED, EVT_NO_CHIPS_TO_FIRE, EVT_READY_TO_FIRE, EVT_BUILDING_HIT
from pyknic.ai.statemachines import StateDrivenAgentBase, BaseState

__version__ = '1.0.0.0'

# for easy comparison as in sys.version_info but digits only
__version_info__ = tuple([int(d) for d in __version__.split('.')])

__author__ = "DR0ID"
__email__ = "dr0iddr0id {at} gmail [dot] com"
__copyright__ = "DR0ID @ 2019"
__credits__ = ["DR0ID"]  # list of contributors
__maintainer__ = "DR0ID"
__license__ = "New BSD license"

# __all__ = []  # list of public visible parts of this module

logger = logging.getLogger(__name__)
logger.debug("importing...")


class TimeIndicator(object):
    kind = KIND_TIME_INDICATOR

    def __init__(self, game_clock, position, duration):
        self.position = position
        self.duration = duration
        self.due_time = game_clock.time + duration
        game_clock.event_update += self._on_time_update
        self.game_clock = game_clock
        self.completed = 0.0  # in percent, [0.0, 1.0]

    def _on_time_update(self, dt, sim_time, game_clock):
        if sim_time >= self.due_time:
            game_clock.event_update -= self._on_time_update
            self.completed = 1.0
            event_dispatcher.fire(EVT_TIME_INDICATOR_REMOVED, self)
            self.game_clock.event_update -= self._on_time_update
        else:
            self.completed = 1.0 - (self.due_time - sim_time) / self.duration


class BuildingBaseState(BaseState):

    @staticmethod
    def on_timer(owner):
        raise NotImplementedError()

    @staticmethod
    def on_consume_energy(owner):
        if owner.energy_pool.amount < owner.template.energy_consumption:
            owner.state_machine.switch_state(NoPower)
        else:
            owner.energy_pool.amount -= owner.template.energy_consumption


class Constructing(BuildingBaseState):

    @staticmethod
    def enter(owner):
        owner.timer.interval = owner.template.build_time
        owner.timer.start()

        owner.chips_prod_timer.stop()
        owner.energy_consume_timer.stop()
        owner.energy_prod_timer.stop()

    @staticmethod
    def exit(owner):
        owner.timer.stop()
        owner.on_built()

    @staticmethod
    def on_timer(owner):
        owner.state_machine.switch_state(BuildingIdle)


class BuildingIdle(BuildingBaseState):

    @staticmethod
    def enter(owner):
        if owner.template.crypto_chips_earn_rate > 0:
            owner.chips_prod_timer.start()
        if owner.template.energy_consumption > 0:
            owner.energy_consume_timer.start()
        if owner.template.energy_production > 0:
            owner.energy_prod_timer.start()
        owner.energy_pool.capacity += owner.template.capacity


class NoPower(BuildingBaseState):

    @staticmethod
    def enter(owner):
        event_dispatcher.fire(EVT_OUT_OF_POWER, owner)

    @staticmethod
    def exit(owner):
        event_dispatcher.fire(EVT_POWER_RESTORED, owner)

    @staticmethod
    def on_consume_energy(owner):
        if owner.energy_pool.amount > owner.template.energy_consumption:
            owner.energy_pool.amount -= owner.template.energy_consumption
            owner.state_machine.switch_state(BuildingIdle)


class Destroyed(BuildingBaseState):

    @staticmethod
    def enter(owner):
        owner.timer.stop()
        owner.chips_prod_timer.stop()
        owner.energy_consume_timer.stop()
        owner.energy_prod_timer.stop()
        owner.on_destroyed()
        owner.energy_pool.capacity -= owner.template.capacity
        if owner.energy_pool.amount > owner.energy_pool.capacity:
            owner.energy_pool.amount = owner.energy_pool.capacity


class Building(StateDrivenAgentBase):

    def __init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                 energy_pool, chips_pool):

        StateDrivenAgentBase.__init__(self, Constructing)
        self.position = position
        self.level = template.level
        self.kind = template.kind
        self.full_health = template.health
        self.health = self.full_health  # this will change
        self.radius = template.radius  # needed by bin space
        self.template = template

        self.energy_pool = energy_pool
        self.chips_pool = chips_pool

        self.timer = timer
        self.timer.repeat = True
        self.timer.event_elapsed += self._on_timer

        def _on_update_chips(*args):
            self.chips_pool.amount += self.template.crypto_chips_earn_rate

        self.chips_prod_timer = chips_prod_timer
        self.chips_prod_timer.interval = 1.0
        self.chips_prod_timer.repeat = True
        self.chips_prod_timer.event_elapsed += _on_update_chips

        self.energy_consume_timer = energy_consume_timer
        self.energy_consume_timer.interval = 1.0
        self.energy_consume_timer.repeat = True
        self.energy_consume_timer.event_elapsed += self._on_consume_energy

        self.state_machine.event_switched_state += self._on_state_switched

        event_dispatcher.add_listener(EVT_QUIT, self._on_quit)

        def _update_energy(*args):
            new_amount = self.energy_pool.amount + self.template.energy_production
            if new_amount < self.energy_pool.capacity:
                self.energy_pool.amount = new_amount
            else:
                logger.info("too much energy produced {0}", new_amount - self.energy_pool.amount)

        self.energy_prod_timer = energy_prod_timer
        self.energy_prod_timer.interval = 1.0
        self.energy_prod_timer.repeat = True
        self.energy_prod_timer.event_elapsed += _update_energy

        Constructing.enter(self)

    def _on_quit(self, *args):
        self.timer.stop()
        self.energy_prod_timer.stop()
        self.energy_consume_timer.stop()
        self.chips_prod_timer.stop()

    def _on_state_switched(self, owner, prev_state, *args):
        logger.info("BB {1} -> {0}", owner.state_machine.current_state, prev_state)

    def on_built(self):
        raise NotImplemented("implement in sub class!")

    def on_destroyed(self):
        raise NotImplemented("implement in sub class!")

    def on_damage(self, hit):
        if self.state_machine.current_state == Constructing:
            return

        self.health -= hit
        event_dispatcher.fire(EVT_BUILDING_HIT, self)
        if self.health <= 0:
            self.health = 0
            self.state_machine.switch_state(Destroyed)

    def on_in_range_enemies_update(self, enemies):
        pass

    def on_upgrade(self, building_type):
        if self.is_in_construction():
            return  # cannot upgrade during upgrade

        def upgrade_action(owner):
            owner.energy_pool.capacity -= owner.template.capacity  # remove the old capacity, added in BuildingIdle
            # update to new values
            owner.level = building_type.level
            owner.kind = building_type.kind
            owner.full_health = building_type.health
            owner.health = owner.full_health  # this will change, reset health at upgrade
            owner.radius = building_type.radius  # needed by bin space
            owner.template = building_type

        self.state_machine.switch_state(Constructing, upgrade_action)

    def is_in_construction(self):
        return self.state_machine.current_state == Constructing

    def _on_timer(self, *args):
        self.state_machine.current_state.on_timer(self)

    def _on_consume_energy(self, *args):
        self.state_machine.current_state.on_consume_energy(self)


class Tower(Building):

    def __init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                 energy_pool, chips_pool):
        Building.__init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                          energy_pool, chips_pool)

        self.targets = set()
        self.bullets = []
        self.bullet_count = 0

    def on_built(self):
        if self.state_machine.current_state == self.state_machine.previous_state:
            event_dispatcher.fire(EVT_TOWER_CREATED, self)
        else:
            # upgrade
            event_dispatcher.fire(EVT_BUILDING_UPGRADED, self)

    def on_destroyed(self):
        event_dispatcher.fire(EVT_TOWER_DESTROYED, self)

    def on_in_range_enemies_update(self, enemies):
        # filter out all enemies that are out of range
        in_range_targets = [e for e in enemies if
                            e.position.get_distance_sq(self.position) < self.template.fire_range ** 2]

        self.targets.intersection_update(in_range_targets)  # filter away all enemies that have moved out of range

        self.targets = set((t for t in self.targets if t.is_alive()))

        # fill up targets if there are more enemies in range than targeted
        diff = self.template.fire_target_count - len(self.targets)
        if diff > 0:
            self.targets.update(in_range_targets[:diff])

        # hack for towers to work
        if self.state_machine.current_state == BuildingIdle and self.targets:
            self.state_machine.switch_state(Shooting)


class Shooting(BuildingBaseState):

    @staticmethod
    def enter(owner):
        owner.timer.interval = 0.01  # initial shot
        owner.timer.start()
        owner.bullet_count = 0

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner):
        owner.targets = set((t for t in owner.targets if t.is_alive()))
        if not owner.targets:
            owner.state_machine.switch_state(BuildingIdle)
            return

        if owner.chips_pool.amount < owner.template.fire_cost:
            owner.state_machine.switch_state(NoCryptoChips)
            return

        owner.chips_pool.amount -= owner.template.fire_cost

        template = owner.template
        owner.timer.interval = 1.0 / template.fire_rate  # set the normal fire rate

        for target in owner.targets:  # target count decides if its a fan or not
            b = Bullet(owner, target, template.bullet_damage, template.bullet_speed, template.bullet_kind)
            owner.bullets.append(b)
        owner.bullet_count += 1

        if owner.bullet_count >= template.fire_cool_count:
            owner.state_machine.switch_state(CoolDown)


class NoCryptoChips(BuildingBaseState):

    @staticmethod
    def enter(owner):
        event_dispatcher.fire(EVT_NO_CHIPS_TO_FIRE, owner)
        owner.timer.interval = 0.5
        owner.timer.start()

    @staticmethod
    def exit(owner):
        owner.timer.stop()
        event_dispatcher.fire(EVT_READY_TO_FIRE, owner)

    @staticmethod
    def on_timer(owner):
        if owner.chips_pool.amount > owner.template.fire_cost:
            owner.state_machine.switch_state(Shooting)


class CoolDown(BuildingBaseState):

    @staticmethod
    def enter(owner):
        owner.timer.interval = owner.template.fire_cool_time
        owner.timer.start()

    @staticmethod
    def exit(owner):
        owner.timer.stop()

    @staticmethod
    def on_timer(owner):
        owner.state_machine.switch_state(Shooting)


class Bullet(object):

    def __init__(self, source, target, damage, speed, kind):
        self.damage = damage
        self.speed = speed
        self.kind = kind
        self.source = source
        self.target = target
        self.position = self.source.position.clone()
        self.position.y -= self.source.template.height

        self.direction = target.position - self.position
        dist = self.direction.length
        self.direction.normalize()

        self.duration = dist / self.speed

    def update(self, dt, sim_time, *args):
        self.position += self.speed * self.direction * dt
        self.duration -= dt
        # if self.target.health <= 0:  # do not remove the bullet if the target died
        #     self.duration = 0

    def impact(self):
        return [self.target.hurt(self.damage)]  # return a list so area 'bullets' are simpler to implement


class UpgradeCenter(Building):

    def __init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                 energy_pool, chips_pool):
        Building.__init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                          energy_pool, chips_pool)

    def on_built(self):
        if self.state_machine.current_state == self.state_machine.previous_state:
            event_dispatcher.fire(EVT_UPGRADE_CENTER_CREATED, self)
        else:
            # upgrade
            event_dispatcher.fire(EVT_BUILDING_UPGRADED, self)

    def on_destroyed(self):
        event_dispatcher.fire(EVT_UPGRADE_CENTER_DESTROYED, self)


class PowerStation(Building):

    def __init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                 energy_pool, chips_pool):
        Building.__init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                          energy_pool, chips_pool)

    def on_built(self):
        if self.state_machine.current_state == self.state_machine.previous_state:
            event_dispatcher.fire(EVT_POWER_STATION_CREATED, self)
        else:
            # upgrade
            event_dispatcher.fire(EVT_BUILDING_UPGRADED, self)

    def on_destroyed(self):
        event_dispatcher.fire(EVT_POWER_STATION_DESTROYED, self)


class ScienceCenter(Building):

    def __init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                 energy_pool, chips_pool):
        Building.__init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                          energy_pool, chips_pool)

    def on_built(self):
        if self.state_machine.current_state == self.state_machine.previous_state:
            event_dispatcher.fire(EVT_SCIENCE_CENTER_CREATED, self)
        else:
            # upgrade
            event_dispatcher.fire(EVT_BUILDING_UPGRADED, self)

    def on_destroyed(self):
        event_dispatcher.fire(EVT_SCIENCE_CENTER_DESTROYED, self)


class ChipsFactory(Building):

    def __init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                 energy_pool, chips_pool):
        Building.__init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                          energy_pool, chips_pool)

    def on_built(self):
        if self.state_machine.current_state == self.state_machine.previous_state:
            event_dispatcher.fire(EVT_CHIPS_FACTORY_CREATED, self)
        else:
            # upgrade
            event_dispatcher.fire(EVT_BUILDING_UPGRADED, self)

    def on_destroyed(self):
        event_dispatcher.fire(EVT_CHIPS_FACTORY_DESTROYED, self)


class Battery(Building):

    def __init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                 energy_pool, chips_pool):
        Building.__init__(self, template, position, timer, chips_prod_timer, energy_consume_timer, energy_prod_timer,
                          energy_pool, chips_pool)

    def on_built(self):
        if self.state_machine.current_state == self.state_machine.previous_state:
            event_dispatcher.fire(EVT_BATTERY_CREATED, self)
        else:
            # upgrade
            event_dispatcher.fire(EVT_BUILDING_UPGRADED, self)

    def on_destroyed(self):
        event_dispatcher.fire(EVT_BATTERY_DESTROYED, self)


logger.debug("imported")
