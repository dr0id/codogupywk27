# -*- coding: utf-8 -*-
"""pygame_boilerplate.py - boilerplate for a simple game prototype"""

from math import radians, cos, sin
import sys

import pygame
from pygame.locals import *

from vec2d import Vec2d

if sys.version_info[0] == 3:
    from functools import reduce
    xrange = range


class Settings:
    resolution = 1024, 768
    grid_scale = 9 * 5


# >>> NOTE <<<
# this simulates the game's settings.py in this code; the values go in local class Settings
settings = Settings


def point_on_circumference(center, radius, degrees_):
    """Calculate the point on the circumference of a circle defined by center and
    radius along the given angle. Returns a tuple (x,y).

    The center argument is a representing the origin of the circle.

    The radius argument is a number representing the length of the radius.

    The degrees_ argument is a number representing the angle of radius from
    origin. The angles 0 and 360 are at the top of the screen, with values
    increasing clockwise.
    """
    radians_ = radians(degrees_ - 90)
    x = center[0] + radius * cos(radians_)
    y = center[1] + radius * sin(radians_)
    return x, y


class PolyGeometry(object):
    # collision_type = POLY_TYPE

    def __init__(self, points, position=None):
        super(PolyGeometry, self).__init__()

        minx = reduce(min, [x for x, y in points])
        width = reduce(max, [x for x, y in points]) - minx + 1
        miny = reduce(min, [y for x, y in points])
        height = reduce(max, [y for x, y in points]) - miny + 1
        self.rect = pygame.Rect(0, 0, width, height)

        self._position = Vec2d(0.0, 0.0)
        if position is None:
            self.position = self.rect.center
        else:
            self.position = position

        self._points = [(x - minx, y - miny) for x, y in points]

    # entity's collided, static method used by QuadTree callback
    # collided = staticmethod(poly_collided_other)

    def getpoints(self):
        left, top = self.rect.topleft
        return [(left + x, top + y) for x, y in self._points]
    points = property(getpoints)

    def getposition(self):
        """GOTCHA: Something like "poly_geom.position.x += 1" will not do what
        you expect. That operation does not update the rect instance variable.
        Instead use "poly_geom.position += (1,0)".
        """
        return self._position

    def setposition(self, val):
        p = self._position
        p.x, p.y = val
        self.rect.center = round(p.x), round(p.y)
    position = property(getposition, setposition)


class HexGeom(PolyGeometry):
    __points = None

    def __init__(self, position):
        if HexGeom.__points is None:
            s = settings.grid_scale
            r = s / 2.0
            p = position
            c = point_on_circumference
            points = []
            for a in range(30, 360, 60):
                points.append(c(p, r, a))
            HexGeom.__points = points
        super(HexGeom, self).__init__(tuple(self.__points), position)


class Timer(object):
    def __init__(self, interval, func):
        """a simple timer that invokes a function when the time interval elapses

        :param interval: seconds between callbacks
        :param func: function to call
        """
        self.interval = interval
        self.func = func
        self.remaining = self.interval

    def update(self, dt):
        self.remaining -= dt
        if self.remaining <= 0.0:
            self.remaining += self.interval
            self.func()


class Game(object):
    def __init__(self):
        """a basic game context with a metered run loop, caption, event handling, game update, and game rendering

        Example usage:
            pygame.init()
            pygame.display.set_mode((1024, 768))
            Game().run()
        """
        self.screen = pygame.display.get_surface()
        self.screen_rect = self.screen.get_rect()
        self.clear_color = Color('darkblue')

        self.clock = pygame.time.Clock()
        self.max_fps = 25
        self.dt = 1.0 / self.max_fps

        self.caption_timer = Timer(1.0, self.update_caption)
        self.timers = [
            Timer(1.0, self.update_caption),
        ]

        self.running = False

        self.sprites = pygame.sprite.Group()
        self.grid = []

        self.font = pygame.font.SysFont('fixed', 10, True)

    def start(self):
        s = settings.grid_scale
        row = 0
        y = s / 2.0
        # Why 7/8? Lol :) Must be the perpendicular of the triangle is that much shorter. /|\
        offx, offy = point_on_circumference((0, 0), s * 7.0 / 8.0, 120)
        while y < self.screen_rect.h:
            x = offx if row % 2 else 0
            col = 0
            while x < self.screen_rect.w:
                hex_node = HexGeom((x, y))
                name = '{},{}'.format(row, col)
                hex_node.name = str(name)
                hex_node.display = self.font.render(str(name), True, Color('yellow'))
                self.grid.append(hex_node)
                x += offx * 2
                col += 1
            y += offy
            row += 1

    def run(self):
        """run loop"""
        self.running = True
        self.start()
        while self.running:
            self.clock.tick(self.max_fps)
            for timer in self.timers:
                timer.update(self.dt)
            self.handle_events()
            self.update(self.dt)
            self.draw()

    def update(self, dt):
        """one frame to update the game model"""
        self.sprites.update(dt)

    def draw(self):
        """one frame to render the game"""
        screen = self.screen
        grid_color = Color('grey')
        screen.fill(self.clear_color)
        for g in self.grid:
            pygame.draw.polygon(screen, grid_color, g.points, 1)
            r = g.display.get_rect(center=g.position)
            screen.blit(g.display, r)
        self.sprites.draw(screen)
        pygame.display.flip()

    def update_caption(self):
        """update the caption in the title bar"""
        pygame.display.set_caption('{} fps'.format(int(self.clock.get_fps())))

    def handle_events(self):
        """handle incoming events"""
        for e in pygame.event.get():
            if e.type == KEYDOWN:
                if e.key == K_ESCAPE:
                    self.running = False
            elif e.type == QUIT:
                self.running = False


if __name__ == '__main__':
    pygame.init()
    pygame.display.set_mode(Settings.resolution)
    Game().run()
